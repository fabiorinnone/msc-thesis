\chapter{Panoramica di NetMatch*}
\vspace{4cm}

In questo capitolo sarà presentata una panoramica generale di NetMatch* e delle sue principali
funzionalità.

NetMatch* è un'app Cytoscape, compatibile con tutte le versioni del software a partire dalla 
versione 3.1. È basata sul plugin NetMatch \cite{ferro2006netmatch}, compatibile con le versioni 2.x 
di Cytoscape, di cui eredità alcune delle sue funzionalità. Cytoscape \cite{shannon2003cytoscape} è 
un software open-source per la visualizzazione e l'analisi di reti d'interazione molecolare, la cui
caratteristica peculiare è la flessibilità: infatti Cytoscape consente a 
ricercatori e sviluppatori di poter adattare l'applicazione alle proprie specifiche esigenze grazie
alla possibilità di implementare app esterne, sviluppate in linguaggio Java, fruibili attraverso 
un App Store \cite{lotia2013cytoscape} online.

In \figurename~\ref{fig:netmatchmain} è mostrata la schermata principale di NetMatch*, in esecuzione
su Cytoscape 3.4.0.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/netmatch.png}
\end{center}
\caption[Schermata principale di NetMatch*]{Schermata principale di NetMatch*: nel pannello di 
controllo di Cytoscape è caricata l'interfaccia principale di NetMatch*; nel pannello dei 
risultati a destra è mostrato il risultato dell'esecuzione di un'operazione di matching.}
\label{fig:netmatchmain}
\end{figure}

\section{Installazione}
Per installare NetMatch* è necessario avviare Cytoscape 3.x e selezionare l'opzione 
``App Manager'' dal menu ``Apps'': alla comparsa della finestra corrispondente sarà possibile
ricercare l'app tra quelle disponibili nell'App Store ricercandone il nome oppure installarla
manualmente selezionando l'opzione ``Install from File'', dopo aver scaricato precedentemente
il file jar eseguibile (\figurename~\ref{fig:install2}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/install2.png}
\end{center}
\caption[Pannello di installazione delle app di Cytoscape]
{Pannello di installazione delle app di Cytoscape.}
\label{fig:install2}
\end{figure} 

Al termine della procedura di installazione, per verificare se sia andata a buon fine è
sufficiente selezionare dalla stessa finestra la scheda ``Currently installed'' e verificare
la presenza di NetMatch* (\figurename~\ref{fig:install3}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/install3.png}
\end{center}
\caption[Elenco delle app installate su Cytoscape]
{Elenco delle app installate su Cytoscape.}
\label{fig:install3}
\end{figure}

\section{Opzioni principali}
Al termine della procedura di installazione per avviare l'app è necessario selezionare 
la voce corrispondente dal menu ``Apps''. All'avvio il pannello principale dell'app sarà caricato
in corrispondenza del pannello di controllo di Cytoscape. L'interfaccia principale di NetMatch*
è costituita da un riquadro con tre schede:

\begin{itemize}
\item la prima scheda, denominata ``Matching'', consente di specificare la reti query e target 
e di avviare la ricerca dei match;
\item la seconda scheda, denominata ``Significance'', consente di verificare la significatività
statistica della query in accordo con uno specifico modello di randomizzazione;
\item La terza scheda, denominata ``Motif library'', consente infine un insieme di query predefinite
che possono essere passate al task di ricerca.
\end{itemize}

In \figurename~\ref{fig:panels} sono mostrate le tre schede principali di NetMatch*.

\begin{figure}[!h]
\centering
\subfigure[]{\includegraphics[scale=0.4]{figures/matchingPanel.png}}\qquad
\subfigure[]{\includegraphics[scale=0.4]{figures/significancePanel.png}}\qquad
\subfigure[]{\includegraphics[scale=0.4]{figures/libraryPanel.png}}\qquad
\label{fig:netmatch}
\caption[Schede del pannello principale di NetMatch*]{Schede del pannello principale di NetMatch*: 
il pannello ``Matching'' (a); il pannello ``Significance'' (b); il pannello ``Motif library'' (c).}
\label{fig:panels}
\end{figure}

\section{Caricamento dei dati di input}
Dal pannello principale ``Matching'' è possibile caricare le reti query e target desiderate.
Selezionando il pulsante ``Load New Network'' è possibile caricare una rete in vari format, tra cui Simple
Interaction Format (SIF) \cite{cytoscapemanualsif}, GML \cite{himsolt1997gml}, XGMML \cite{punin2001xgmml}, 
GraphML \cite{brandes2013graph} e molti altri. 

%In \figurename~\ref{fig:targetNetwork} è mostrata la rete di interazione GAL che descrive l'utilizzo
%del galattosio nel lievito \emph{Saccharomyces cerevisiae} \cite{ideker2001integrated}.

%\begin{figure}[!h]
%\begin{center}
%\includegraphics[width=14cm]{figures/targetNetwork.png}
%\end{center}
%\caption[Rete di interazione GAL caricata su NetMatch*]
%{Rete di interazione GAL caricata su NetMatch*.}
%\label{fig:targetNetwork}
%\end{figure}

Il caricamento di una rete può essere normalmente effettuato mediante le opzioni di Cytoscape: in
ogni caso, al termine del caricamento, le reti saranno aggiunte alla lista delle reti caricate su
Cytoscape e compariranno selezionalibili come query o target nel pannello principale di NetMatch*.

La schermata principale di NetMatch*, denominata ``Matching'' (\figurename~\ref{fig:panels}(a))
consente all'utente di impostare la rete query e la rete target su cui ricercare le occorrenze.
Dal sottopannello ``Query Properties'',  selezionando i menu a tendina ``Query'' e ``Target Network`` 
è possibile selezionare rispettivamente la rete query e la rete target desiderate tra quelle
correntemente caricate nel progetto corrente. Opzionalmente l'utente può modificare le reti aggiungendo 
nodi o archi o modificandone le etichette o  tramite il pannello di visualizzazione della network 
o editando le informazioni mostrate nel pannello inferiore di Cytoscape (``Table Panel'').

\section{Ricerca delle sottostrutture}
Il pannello ``Matching'' di NetMatch* è costituito da due ulteriori sottopannelli che consentono
all'utente di impostare ulteriori opzioni:

\begin{itemize}
\item il sottopannello ``Graph Properties'' consente di impostare se, in fase di ricerca dei
match, è necessario tenere conto delle etichette dei vertici e degli archi (opzione 
``Labeled'') e se considerare i grafi come orientati o non orientati (opzione ``Directed'');
\item il sottopannello ``Algorithm'' consente invece di scegliere l'algoritmo di matching da
utilizzare: le opzioni possibili sono l'algoritmo RI e la sua variante RI-DS.
\end{itemize}

\section{Disegnare una query}
NetMatch* offre la possibilità di creare ed editare delle nuove query: per effettuare questa
operazione, dal pannello principale, è necessario selezionare il pulsante ``Create New Query
Network''. Nel pannello principale di Cytoscape verrà aggiunto un nuovo pannello per la creazione
di una nuova query. Cliccando con il tasto destro sul pannello si aprirà il menu standard di
Cytoscape che consente di aggiungere, modificare o rimuovere elementi del grafo, quali
nodi (\figurename~\ref{fig:drawing2}) e archi (\figurename~\ref{fig:drawing3}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/drawing2.png}
\end{center}
\caption[Menu di aggiunta di un nuovo nodo al grafo query]
{Menu di aggiunta di un nuovo nodo al grafo query.}
\label{fig:drawing2}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/drawing3.png}
\end{center}
\caption[Menu di aggiunta di un nuovo arco al grafo query]
{Menu di aggiunta di un nuovo arco al grafo query.}
\label{fig:drawing3}
\end{figure}

Dopo aver aggiunto i nodi e gli archi desiderati, selezionando un nodo e cliccando
nuovamente con il tasto destro del mouse, comparirà il menu che consente di settare le opzioni
dell'elemento del grafo selezionato (\figurename~\ref{fig:drawing4}): in questo caso comparirà 
un sottomenu denominato ``NetMatch*'' che darà la possibilità all'utente di modificare le 
etichette del nodo selezionato.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/drawing4.png}
\end{center}
\caption[Sottomenu delle opzioni di NetMatch* relative ai nodi]
{Sottomenu delle opzioni di NetMatch* relative ai nodi.}
\label{fig:drawing4}
\end{figure}

Selezionando l'opzione corrispondende l'utente potrà decidere eventualmente di impostare
un'etichetta per il nodo selezionato (\figurename~\ref{fig:drawing6}). In caso contrario 
il nodo sarà considerato non etichettato. Come impostazione di default i nodi sono creati come 
non etichettati. In realtà, per definire un nodo non etichettato viene associato ad esso uno 
speciale attributo jolly ``?'' che  Cytoscape interpreta come carattere, ma che NetMatch* 
in fase di matching interpreterà come assenza di etichetta impostata. Qualsiasi altro 
carattere o stringa invece sarà interpretato come un'etichetta.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/drawing6.png}
\end{center}
\caption[Schermata di impostazione di un'etichetta per un nodo]
{Schermata di impostazione di un'etichetta per un nodo.}
\label{fig:drawing6}
\end{figure}

Selezionando, invece, un arco e cliccando con il tasto desto del mouse comparirà un menu analogo
al precedente, con la differenza che il sottmenu denominato ``NetMatch*'' conterrà due differenti
opzioni: la prima consente di modificare l'etichetta dell'arco selezionato, la seconda di 
impostare un cammino approssimato (\figurename~\ref{fig:drawing7}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/drawing7.png}
\end{center}
\caption[Sottomenu delle opzioni di NetMatch* relative agli archi]
{Sottomenu delle opzioni di NetMatch* relative agli archi.}
\label{fig:drawing7}
\end{figure}

La schermata di impostazione delle etichette degli archi è analoga a quella dei nodi. Tuttavia
nel caso degli archi è possibile anche impostare i cammini approssimati tra due nodi $v_i$ e $v_j$.
(\figurename~\ref{fig:drawing9}). I cammini sono definiti tramite speciali 
attributi per l'arco $(v_i,v_j)$: in particolare la lunghezza dell'arco può essere definita mediante
un'espressione della forma $a\textrm{ op }b$, dove $a$ e $b$ sono due numeri interi o il carattere jolly
``?'' e op è un'operatore logico booleano a scelta tra $<,\leq,\geq,>,=$. In questo caso il
carattere jolly ``?'' indica che la lunghezza del cammino non è specificata e pertanto il cammino
è detto approssimato. Per esempio, l'espressione $? \leq 2$ significa che il corrispondente cammino 
deve avere lunghezza al massimo pari a 2, mentre $? > 3$ corrisponde ad un cammino la cui lunghezza
è strettamente maggiore di 3. Una query che contiene un carattere jolly ``?'' in almeno un nodo
o un arco o in un cammino viene interpretata da NetMatch* come una query approssimata e trattata
opportunamente. Un esempio di query approssimata è mostrato in \figurename~\ref{fig:approxQuery}.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/drawing9.png}
\end{center}
\caption[Schermata di impostazione di un cammino fra una coppia di nodi]
{Schermata di impostazione di un cammino fra una coppia di nodi.}
\label{fig:drawing9}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=10cm]{figures/approxQuery.png}
\end{center}
\caption[Esempio di query approssimata]
{Esempio di query approssimata: la query contiene 3 nodi e 3 archi, di cui 2 nodi hanno una
specifica etichetta e un arco rappresenta un cammino approssimato di lunghezza pari ad almeno 2.
I rimanenti elementi del grafo non hanno etichette specificate.}
\label{fig:approxQuery}
\end{figure}

Cliccando sul pulsante ``Save Query Network'' è possibile salvare sul proprio file system la 
query creata in un formato SIF personalizzato in cui gli attributi dei nodi e degli archi sono
memorizzati in appositi file con estensioni rispettivamente .NA e .EA.

Nelle figure \ref{file_na} e \ref{file_ea} sono mostrati due esempi di file degli 
attributi associabili ad un file in formato SIF.

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{node1 = nattr1\\
node2 = nattr2\\
node3 = nattr3\\
node4 = nattr4\\
node5 = nattr5}
\end{minipage}}}}
\caption[File .NA con attributi dei nodi di un file SIF]{File .NA con attributi dei nodi di un 
file SIF: ad ogni nodo è associato un solo attributo.}
\label{file_na}
\end{figure}

\begin{figure}[!h]
\centering{\scalebox{1}{\fbox{%
\begin{minipage}{1\linewidth}
\linespread{1.0}
\texttt{node1 (edge1) node2 = eattr1\\
node1 (edge1) node3 = eattr2\\
node2 (edge2) node3 = eattr3\\
node4 (edge3) node4 = eattr4\\
node4 (edge3) node5 = eattr5}
\end{minipage}}}}
\caption[File .EA con attributi degli archi di un file SIF]{File .EA con attributi degli archi di 
un file SIF: per ogni arco con specifica etichetta, avente nodo sorgente e destinazione definiti, 
è associato un attributo.}
\label{file_ea}
\end{figure}

\section{Query predefinite}\label{sec:significance}
Selezionando la scheda ``Motifs library'' (\figurename~\ref{fig:panels}(b)) è possibile accedere 
ad un insieme predefinito di query selezionabili rapidamente che includono alcune topologie di 
piccole dimensioni che possono essere identificate come motivi \cite{milo2002network} in molte 
reti reali. I motivi supportati da NetMatch* (\figurename~\ref{fig:motifs}) sono il three-chain, 
il feed-forward loop \cite{mangan2003structure}, il bi-parallel, il bi-fan e la m-to-n-fan.

Dato un grafo $G=(V,E)$ e un motivo $M$, il suo significato statistico tipicamente viene descritto da 
due punteggi chiamati rispettivamente \textit{e-value}, calcolato nel seguente modo:

\begin{displaymath}
e_M=\frac{n^{rand}}{n_M}
\end{displaymath}

dove $n^{rand}$ è il numero di occorrenze di $M$ nella rete random e $n_M$ è il numero di occorrenze
del sottografo $M$ in $G=(V,E)$, e lo \textit{Z-score} \cite{kashtan2004efficient}, calcolato invece 
nel seguente modo:

\begin{displaymath}
Z_M=\frac{n_M-\langle n_M^\text{rand}\rangle}{\sigma_{n_M}^{\text{rand}}}
\end{displaymath} 

dove $\langle n_M^{\text{rand}}\rangle$ e $\sigma_{n_M}^{rand}$ sono rispettivamente la 
media e la deviazione standard del numero di volte che il sottografo $M$ compare in
una rete aleatoria.

Cliccando sul pulsante corrispondente alla rete desiderata, quest'ultima sarà aggiunta come nuova 
query al pannello principale di Cytoscape ed eventualmente modificata con lo stesso approccio 
descritto nel paragrafo precedente.

\begin{figure}[!h]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.75, shorten >= 1pt, auto, node distance = 3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white]
	\tikzstyle{edge_style} = [->, draw = black, semithick]]
	\node[node_style] (v1) at (0,2) {?};
	\node[node_style] (v2) at (2,0) {?};
	\node[node_style] (v3) at (0,-2) {?};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.75, shorten >= 1pt, auto, node distance = 3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (0,2) {?};
	\node[node_style] (v2) at (-2,0) {?};
	\node[node_style] (v3) at (2,0) {?};
	\node[node_style] (v4) at (0,-2) {?};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v2) edge (v4);
	\draw[edge_style] (v3) edge (v4);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.75, shorten >= 1pt, auto, node distance = 3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (0,2) {?};
	\node[node_style] (v2) at (2,0) {?};
	\node[node_style] (v3) at (0,-2) {?};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v1) edge (v3);
	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale=0.75, shorten >=1pt, auto, node distance=3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (-2,2) {?};
	\node[node_style] (v2) at (2,2) {?};
	\node[node_style] (v3) at (-2,-2) {?};
	\node[node_style] (v4) at (2,-2) {?};
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v2) edge (v4);
 	\end{tikzpicture}}
\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale=0.75, shorten >=1pt, auto, node distance=3cm, semithick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white]
	\tikzstyle{empty_style} = [draw = white, fill = white]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[empty_style] at (0,2) {\ldots m \ldots};
	\node[empty_style] at (0,-2) {\ldots n \ldots};
	\node[node_style] (v1) at (-2,2) {?};
	\node[node_style] (v2) at (2,2) {?};
	\node[node_style] (v3) at (-2,-2) {?};
	\node[node_style] (v4) at (2,-2) {?};
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v2) edge (v4);
 	\end{tikzpicture}}
\caption[Query predefinite di NetMatch*]
{Query predefinite di NetMatch*: three-chain (a); bi-parallel (b); feed-forward loop (c);
bi-fan (d); m-to-n-fan (e).}
\label{fig:motifs}
\end{figure}

\section{Gestione dei risultati}
Dopo aver effettuato il caricamento delle reti query e target, attraverso il pannello ``Matching''
è possibile impostare le opzioni di ricerca ed avviare il task di ricerca dei match. Come primo passo
l'utente deve selezionare dal relativo menu a tendina la rete target (\figurename~\ref{fig:results2})
ed opzionalmente la rete query, se l'utente ne ha caricate o create più di una.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/results2.png}
\end{center}
\caption[Selezione della rete target]
{Selezione della rete target.}
\label{fig:results2}
\end{figure}

A questo punto l'utente deve selezionare, nel caso in cui ha attivato l'opzione
``Labeled'' dal pannello ``Query Properties'', dai relativi menu a tendina, rispettivamente
gli attributi dei nodi (\figurename~\ref{fig:results3}) e degli archi (\figurename~\ref{fig:results4}).
Se l'utente ha caricato da un file esterno una query o ha creato manualmente una query alla quale 
ha successivamente effettuato modifiche alla tabella degli attributi, può ripetere la stessa operazione
anche per la rete query. In caso contrario saranno usati gli attributi predefiniti della rete query.
Qualora l'opzione ``Labeled'' non sia selezionata, la selezione degli attributi della rete target
(o della rete query) non sortirà nessun effetto nella fase di ricerca. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/results3.png}
\end{center}
\caption[Selezione degli attributi dei nodi della rete target]
{Selezione degli attributi dei nodi della rete target.}
\label{fig:results3}
\end{figure}

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/results4.png}
\end{center}
\caption[Selezione degli attributi degli archi della rete target]
{Selezione degli attributi degli archi della rete target.}
\label{fig:results4}
\end{figure}

A questo punto l'utente può avviare la ricerca cliccando sul pulsante ``Match'': NetMatch* 
avvierà quindi il task di ricerca degli eventuali match (\figurename~\ref{fig:matching}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/matching.png}
\end{center}
\caption[Ricerca dei match]
{Ricerca dei match.}
\label{fig:matching}
\end{figure}

Al termine del task di ricerca i risultati saranno mostrati nel pannello laterale a destra
di Cytoscape (``Results Panel''). Per ogni occorrenza individuata sarà mostrata graficamente
la sua topologia e l'elenco dei nodi e degli archi coinvolti (\figurename~\ref{fig:results5}). 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/results5.png}
\end{center}
\caption[Visualizzazione dei risultati]
{Visualizzazione dei risultati.}
\label{fig:results5}
\end{figure}

L'utente può selezionare dal pannello dei risultati un qualunque match che sarà evidenziato
nella rete target (\figurename~\ref{fig:results6}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/results6.png}
\end{center}
\caption[Selezione di un singolo match]
{Selezione di un singolo match.}
\label{fig:results6}
\end{figure}

Qualora l'utente selezioni l'opzione ``Create a child network'', alla selezione del match
sarà creata e visualizzata una nuova rete avente la topologia del match selezionato
(\figurename~\ref{fig:results7}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/results7.png}
\end{center}
\caption[Selezione di un singolo match come una nuova rete]
{Selezione di un singolo match come una nuova rete.}
\label{fig:results7}
\end{figure}

\section{Significatività}\label{sec:significance}
Selezionando la scheda ``Significance'' (\figurename~\ref{fig:panels}(c)) si accede ad una
scheda contenente i parametri per la valutazione della significatività statistica di un motivo.
La scheda consiste di tre sottopannelli:

\begin{itemize}
\item il sottopannello superiore denominato ``Options'' consente di selezionare il numero di grafi
casuali da generare per effettuare i test statistici (da 0 a 100). Inoltre è possibile 
opzionalmente indicare il seme per la generazione dei numeri pseudocasuali;
\item il sottopannello centrale denominato ``Metrics'' consente all'utente di calcolare alcune 
metriche relative al grafo target e ai grafi casuali generati, per ciascuno modello di randomizzazione;
\item il sottopanello inferiore denominato ``Models'' consente, infine, di selezionare il modello
di randomizzazione desiderato e di settarne le impostazioni principali, nonché di avviare il test
statistico cliccando sul pulsante ``Start''.
\end{itemize} 

Il calcolo delle metriche comprende come output il grado medio, il coefficiente di clustering
e il coefficiente di assortatività: l'output viene mostrato in una finestra esterna e può essere
salvato opzionalmente in formato CSV (\figurename~\ref{fig:significance2}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/significance2.png}
\end{center}
\caption[Calcolo delle metriche]
{Calcolo delle metriche: per la rete target e per ciascuna rete ottenuta mediante i 7 modelli
di randomizzazione sono mostrati i risultati relativi a grado medio, coefficiente di clustering
e coefficiente di assortatività. I risultati possono essere salvati in formato CSV.}
\label{fig:significance2}
\end{figure}

I modelli di randomizzazione previsti sono 7 e per ciascun modello è possibile settare manualmente
alcune opzioni. Il modello ``Shuffling'' presenta due opzioni: una casella di selezione ``Lab shuffling''
e una casella di testo ``Sw/edg'' ove possibile impostare manualmente un parametro numerico. Il modello
di ``Erdos-Renyi'' non presenta opzioni. Il modello ``Watts-Strogatz`` consente di impostare
la probabilità di riscrittura $\beta$. Il modello ``Barabasi-Albert'' consente di impostare il numero
di nodi iniziali della rete. Il modello ``Duplication'' ha due parametri: ``Init nodes'', ossia
il numero di nodi iniziali della rete e ``Edge prob'', ossia la probabilità di duplicazione di un arco.
Il modello ``Geometric'' ha un solo parametro ``Dim'' che denota la dimensione dello spazio dove
i punti sono impostati. Il modello ``Forest-fire'', infine, ha il parametro ``Ambass'', ossia il 
numero di nodi ambasciatori.

Per ogni modello, i rimanenti parametri sono stimati in base al numero di nodi e di archi della rete
target selezionata. In tutti i casi, ad eccezione del modello basato su shuffling la computazione
è effettuata generando inizialmente una rete con $|V|$ nodi aventi le stesse etichette della rete target 
e senza archi. Successivamente, sono aggiunti archi che collegano i vari nodi fino all'ottenimento
di una nuova rete avente $|V|$ nodi e $|E|$ archi. 

Cliccando sul pulsante ``Start'' è possibile avviare il test di significatività statistica. Il risultato
ottenuto sarà mostrato in un apposito messaggio di dialogo (\figurename~\ref{fig:significance3}).

\begin{figure}[t!]
\begin{center}
\includegraphics[width=14cm]{figures/significance3.png}
\end{center}
\caption[Output del test di significatività statistica]
{Output del test di significatività statistica: sono mostrati rispettivamente il numero di occorrenze
nella rete reale, il numero medio di occorrenze nelle reti casuali generate, la deviazione standard
delle occorrenze nelle reti casuali generate, l'e-value e lo Z-score.}
\label{fig:significance3}
\end{figure}
