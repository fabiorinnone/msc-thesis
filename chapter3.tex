\chapter{Modelli di randomizzazione}\label{cap:chapter3}
\vspace{4cm}

In NetMatch* l'utente può selezionare sette modelli di randomizzazione allo scopo
di calcolare la significatività statistica di un motivo. In questo capitolo tali modelli saranno
presentati in dettaglio, dopo aver definito alcune proprietà fondamentali dei grafi
random.

\section{Proprietà delle reti reali}
Un aspetto molto importante nello studio delle reti complesse è lo studio di modelli matematici
usati per la generazione di reti che quanto più possibile possano rispecchiare proprietà
e caratteristiche delle reti reali. Negli ultimi decenni, infatti, è stato possibile approfondire
empiricamente lo studio di network reali di grandi dimensioni, dimostrando che molte di esse
godono di numerose proprietà comuni. È stato riscontrato che molte di queste reti hanno
un elevato coefficiente di clustering, pertanto la probabilità che due vertici siano vicini
cresce con l'aumentare dei vicini in comune. Molte reti reali sono inoltre a invarianza di scala:
sono quindi reti in cui la distribuzione di grado dei vertici è quasi indipendente dalle dimensioni
del grafo.

%\subsection{Proprietà del piccolo mondo}
È stato osservato inoltre che molte reti reali godono della cosiddetta
proprietà del ``piccolo mondo'', ossia che la maggior parte dei vertici che la compongono 
sono separati da cammini di lunghezza molto brevi rispetto alla dimensione grafo. Tale proprietà
prende spunto dalla cosiddetta teoria del piccolo mondo, ossia una branca della teoria dei grafi nata
grazie ad esperimenti empirici attuati da Stanley Milgram \cite{milgram1967small,travers1969experimental}, 
conseguenza dell'esame della lunghezza media di un qualunque percorso all'interno di una rete sociale. 
L'esperimento, noto con l'espressione ``sei gradi di separazione'', afferma che una qualunque 
persona è collegata ad una qualunque altra attraverso una rete di conoscenze e relazioni con non più 
di $5$ intermediari.

Più in generale la proprietà del piccolo mondo è una proprietà delle reti reali per la quale,
indipendentemente dalla grandezza della rete, i cammini minimi tra due nodi scelti casualmente
sono relativamente brevi rispetto alla dimensione della rete. In particolar modo la lunghezza
media dei cammini minimi dipende logaritmicamente dalla dimensione della rete: questa
proprietà è osservata tipicamente nelle reti biologiche e tecnologiche.

Formalmente la proprietà del piccolo mondo è contraddistinta da un cammino caratetteristico piccolo 
che varia logaritmicamente rispetto a $|V|$:

\begin{displaymath}
L\approx\ln |V|
\end{displaymath}

Inoltre le reti che godono di questa proprietà sono caratterizzate da un elevato coefficiente
di clustering \cite{watts1998collective}. 

\section{Reti a invarianza di scala}
Una caratteristica che è stata riscontrata nelle reti reali è che la distribuzione del grado dei
suoi nodi mostra tipicamente un andamento che rispetta una legge di potenza. Le reti che rispettano 
la legge di potenza sono dette reti a invarianza di scala o \emph{scale-free}
\cite{albert2002statistical}.

In particolare, la legge di potenza è una funzione del tipo:

\begin{displaymath}
f(x) = ax^k + o(x^k),
\end{displaymath}

dove $a,k$ sono costanti e $o(x^k)$ l'esponente di scala, ossia una funzione asintoticamente
piccola di $x^k$.

Una distribuzione di probabilità è detta \textit{a legge di potenza} se è della forma:

\begin{displaymath}
p(x) \propto L(x)x^{-\alpha},
\end{displaymath}

dove $L(x)$ è una funzione che ``varia lentamente'', $\alpha$ è l'esponente della legge di 
potenza e inoltre vale:

\begin{displaymath}
\lim_{x\to\infty}\frac{L(tx)}{L(x)}=1
\end{displaymath}

con $t$ costante.

Nello specifico, in molte reti reali è stato osservato che la distribuzione di grado rispetta una 
legge di potenza del tipo:

\begin{equation}
\label{eq:scale-free}
P(k)\sim ak^{-\beta}
\end{equation}

dove $a$ è una costante e $\beta$ è un esponente tipicamente compreso nell'intervallo $2<\beta <3$
\cite{boccaletti2006complex}. 

Nelle reti ad invarianza di scala non esiste pertanto una ``scala'' caratteristica per il grado $k$: 
questo significa che non esiste un valore medio di riferimento attorno al quale il valore del grado 
fluttua con piccoli scostamenti, ma piuttosto vi è un ampio range di variabilità. Nelle reti ad 
invarianza di scala, a causa delle proprietà della distribuzione di grado $P(k)$, il valore del grado 
medio ha pertanto poca rilevanza. 

Tali reti, inoltre, sono caratterizzate dalla presenza di pochi hubs aventi un alto grado 
connessi ad un elevato numero di nodi aventi basso grado, a differenza di modelli ideali in 
cui i nodi sono tutti topologicamente equivalenti e caratterizzati da un grado omogeneo. 

\section{I grafi regolari}
La teoria dei grafi propone una soluzione matematica formale per la definizione di modelli
ideali, di derivazione puramente matematica. Tuttavia, le nozione proposte in questa teoria
consentono di poter essere agevolmente adattate alla descrizione di reti reali. Un primo modello
che è possibile adottare a tal scopo è quello dei grafi regolari \cite{barabasi1999emergence}.

Un grafo regolare è un modello ideale di grafo caratterizzato da schemi di connessione ordinati
fra i nodi che lo compongono. Esempi tipici di grafi regolari possono essere gli anelli, le griglie
o i reticoli. La proprietà principale di tali grafi è che ogni nodo ha sempre il medesimo grado $k$.
Ad esempio un anello composto da $n$ nodi è un insieme di nodi disposti circolarmente, in cui
ciascun nodo è connesso simmetricamente ai suoi $2m$ vicini, formando un numero totale di archi $e=mn$.
La scelta del valore $m$ è tipicamente arbitraria, ma deve essere tale che $m\ll n/2$.

In un grafo regolare generalmente le connessioni tra i nodi tendono ad essere ``localmente dense'',
cioè due nodi adiacenti tendono ad avere gli stessi vicini e quindi un alto livello di clustering.
Tuttavia, scelti due nodi a caso la loro distanza può essere molto elevata e il cammino di lunghezza
minima può coinvolgere un numero elevato di archi. 

\section{I grafi aleatori}
Il termine grafo aleatorio o rete random si riferisce alla natura disordinata 
degli archi che collegano i vari nodi di un grafo. La teoria dei grafi aleatori prende le basi
dall'assunzione che la connessione fra due nodi che la compongono sia casuale, ossia che esista
una probabilità che un'arco esista o meno e che non vi sia un ordine nella distribuzione
dei collegamenti.

Tipicamente un grafo aleaorio viene generato a partire da $n_0$ nodi iniziali, successivamente interconnessi 
fra loro da archi inseriti casualmente secondo una determinata distribuzione di probabilità. 
Tuttavia, in alcuni casi, si è interessati ai cosiddetti processi di grafi aleatori, pertanto non al 
grafo in sé, ma al processo stocastico che descrive l'evoluzione del grafo aleatorio nel tempo.

La teoria sui grafi aleatori fu sviluppata originariamente da nel Paul Erd\H{o}s nel
1947 \cite{erdos1947some} e successivamente ampliata dallo stesso Erd\H{o}s e da 
Alfr\'{e}d R\'{e}nyi \cite{erdos1959random}: il loro obiettivo principale era, originariamente, 
quello di dimostrare, avvalendosi di metodi probabilistici, che esistessero specifiche proprietà 
valide per quasi tutti i grafi reali. In particolare, i due matematici considerarono per la
prima volta la realtà, ed i grafi che avrebbero dovuto rappresentarla, del tutto basati
sul caso. Questa assunzione è stata la via più semplice e compatta per fornire una chiave
interpretativa fra i vari componenti di un sistema complesso. 

\section{Modello basato su shuffling}
Il modello basato su shuffling \cite{milo2004uniform} consente di generare grafi aleatori, 
denotati come $G_{Sh}=(V,E)$, ottenuti ``ricablando'' un grafo di input scambiando ripetutamente 
i nodi target di due archi scelti di volta in volta casualmente, quando questa operazione è possibile. 
Opzionalmente è possibile scambiare casualmente anche le etichette degli archi, qualora presenti. 

Dato un grafo $G=(V,E)$ l'algoritmo seleziona di volta in volta causalmente una coppia di archi
$((v_i,v_j),(v_k,v_l))$ e la sostituisce con la coppia $((v_i,v_l),(v_k,v_j))$, dove
$0\leq i,j,k,l\leq|E|$. Lo scambio viene eseguito solo nel caso in cui non generi archi multipli o 
cappi. Il processo di scambio è ripetuto $q|E|$ volte, dove $q$ è un intero scelto opportunamente 
in modo da non alterare il grado entrante ed uscente dei nodi.

Il risultato dell'esecuzione dell'algoritmo di shuffling produce una rete che preserva la
stessa distribuzione di grado $P(k)$ della rete originale.

\section{Modello di Erd\H{o}s e R\'{e}nyi}
I matematici Erd\H{o}s e R\'{e}nyi proposero originariamente un modello \cite{erdos1959random,
boccaletti2006complex}, detto anche modello ER, in grado di generare grafi aleatori in cui ogni 
arco viene creato in modo indipendente con una probabilità fissata. I grafi generati secondo 
questo modello sono denotati come $G_{ER}=(V,E)$.
 
La generazione del grafo avviene nel seguente modo: inizialmente esso è costituito da $n_0=|V|$ nodi 
disconnessi; successivamente coppie di nodi selezionate casualmente sono collegate fra loro 
tramite archi, in modo da tale da impedire la formazione di archi multipli o cappi, fin quando il 
numero totale di archi non sia $|E|$. 

Un'ulteriore possibile implementazione del modello consiste nel connettere ogni coppia di
nodi con una probabilità $0<p<1$. Questo metodo produce un insieme di grafi aleatori,
denotato come $G_{ER}(p)$, contenente grafi aventi un numero differente di archi: in 
particolare i grafi con esattamente $|E|$ archi appariranno nell'insieme con una probabilità pari 
a $p^k(1-p)^{\frac{|V|(|V|-1)}{2-|E|}}$.

In questo caso un grafo $G_{ER}=(V,E)\in G_{ER}(p)$ con $V=\{v_0,v_1,\ldots, v_n\}$ sarà caratterizzato 
da una matrice di adiacenza $A(a_{ij})$ tale che, per ogni coppia di nodi distinti $(v_i,v_j)$, la 
probabilità che $a_{ij}=1$ è $p$ e la sua diagonale principale è nulla:

\begin{displaymath}
a_{ij} = \left\{
\begin{array}{rl}
1 & \text{con probabilità } p,\\[0.5em]
0 & \text{con probabilità } 1-p\text{ o se }i=j.
\end{array} \right.
\end{displaymath}  

Il modello di Erd\H{o}s e R\'{e}nyi è concettualmente molto semplice, ma non riproduce molte
delle proprietà che caratterizzano le reti reali. Inoltre le proprietà dei grafi generati
secondo questo modello variano considerevolmente in funzione in funzione della probabilità $p$.
Infatti per un valore critico della probabilità $p_c=\frac{1}{|V|}$, si avrà un corrispondente
grado critico del grafo pari a ${\langle k\rangle}_c=1$.

Erd\H{o}s e R\'{e}nyi hanno anche provato che se $p<p_c$ il grafo non ha componenti di 
dimensioni più grandi di $\mathscr{O}(\ln |V|)$ e nessuna componente ha cicli, se $p=p_c$ allora la 
componente più grande del grafo ha dimensione $\mathscr{O}(|V|^{2/3})$ e, infine, se $p>p_c$ 
allora il grafo ha una componente di $\mathscr{O}(|V|)$ nodi con $\mathscr{O}(|V|)$ cicli
e nessun altra componente ha più di $\mathscr{O}(\ln |V|)$ nodi e più di un ciclo.

Per valori grandi di $|V|$ e valore di grado medio $\langle k \rangle$ fissato, la 
distribuzione del grado è approssimata dalla distribuzione di Poisson, ossia:

\begin{displaymath}
P(n) = e^{-\lambda}\frac{\langle k \rangle^k}{k!},
\end{displaymath}

infatti è per questo motivo che i grafi ER sono anche chiamati \textit{Poisson random graphs}.

Inoltre i grafi ER sono grafi non correlati, ossia gli archi sono connessi ai nodi
indipendentemente dal loro grado, il che significa che $P(k'|k)$ e $\langle k_{nn}\rangle$ sono
indipendenti da $k$.

Infine, quando $p\geq\ln{|V|/|V|}$ accade che la maggior parte dei grafi $G\in G_{ER}(p)$ sono completi 
e il loro diametro varia in un range di valori attorno $\text{Diam}(G)=\ln|V|/\ln(p|V|)$. 

In \figurename~\ref{fig:erdosRenyi} è mostrato un esempio di rete generata secondo il modello ER.

\begin{figure}[h]
\begin{center}
\includegraphics[width=13cm]{figures/erdosRenyi}
\end{center}
\caption[Rete aleatoria generata secondo il modello di Erd\H{o}s e R\'{e}nyi]
{Rete aleatoria generata secondo il modello di Erd\H{o}s e R\'{e}nyi. I parametri iniziali
sono $n_0=100$ e $p=0,05$.}
\label{fig:erdosRenyi}
\end{figure}

\section{Modello di Watts e Strogatz}
Il modello di Watts Strogatz \cite{watts1998collective,boccaletti2006complex}, detto anche modello WS, 
consente di generare grafi aleatori che godono della proprietà del piccolo mondo: in particolare i 
grafi generati dal modello, denotati come $G_{WS}=(V,E)$, sono caratterizzati da una lunghezza media 
dei cammini fra le coppie di vertici piccola ed un elevato coefficiente di clustering.

Il modello è di tipo statico, in quanto la struttura del grafo generato non è in relazione con l'evoluzione
e con la crescita del grafo stesso. 

Il modello è basato su una procedura di riconnessione degli archi con probabilità $p$. La probabilità $p$ è
detta appunto di \textit{riconnessione}. Al passo iniziale
il grafo è un anello regolare costituito da $n_0=|V|$ nodi, in cui ogni nodo è connesso simmetricamente 
ai suoi $2m$ più vicini, in modo che il totale degli archi aggiunti sia $m_0=m|V|$. Quindi,
per ogni nodo, l'arco collegato al vicino più immediato in senso orario viene agganciato
ad un altro nodo scelto con una probabilità di riconnessione $p$, oppure mantenuto alla stessa posizione 
con probabilità $1-p$.

È possibile dimostrare che se $p=0$ il grafo è un reticolo regolare, se $p=1$ il grafo generato
è random ed è caratterizzato dalla proprietà per cui ogni suo nodo ha un grado minimo pari a $m$.
Per valori intermedi della probabilità di riconnessione ($p\neq 0$ e $p \neq 1$), i grafi generati secondo
il modello WS godono invece della proprietà del piccolo mondo.

%Sono stati elaborati svariati modelli che consentono di costruire reti casuali che rispecchiano 
%la cosiddetta legge di potenza. Poiché si parla di network la distribuzione a legge di potenza 
%riguarda il grado. In particolare, la legge di potenza è una funzione del tipo:

%\begin{displaymath}
%f(x) = ax^k + o(x^k),
%\end{displaymath}

%dove $a,k$ sono costanti e $o(x^k)$ l'esponente di scala, ossia una funzione asintoticamente
%piccola di $x^k$.

%Una distribuzione di probabilità è detta a legge di potenza se è della forma:

%\begin{displaymath}
%p(x) \propto L(x)x^{-\alpha},
%\end{displaymath}

%dove $L(x)$ è una funzione che ``varia lentamente'', $\alpha$ è l'esponente della legge di 
%potenza e inoltre vale:

%\begin{displaymath}
%\lim_{x\to\infty}\frac{L(tx)}{L(x)}=1
%\end{displaymath}

%con $t$ costante.

%Le reti con distribuzione (di grado) a legge di potenza sono dette anche reti ad invarianza
%di scala (o \textit{scale-free}).

In \figurename~\ref{fig:wattsStrogatz} è mostrato un esempio di rete generata secondo il modello
di Watts Strogatz.

\begin{figure}[h]
\begin{center}
\includegraphics[width=13cm]{figures/wattsStrogatz}
\end{center}
\caption[Rete aleatoria generata secondo il modello di Watts Strogatz]
{Rete aleatoria generata secondo il modello di Watts Strogatz. I parametri iniziali
sono $n_0=100$, $m_0=50$ e $p=0,05$.}
\label{fig:wattsStrogatz}
\end{figure}

Dallo studio dei valori del coefficiente di clustering $C$ e del cammino caratteristico $L$ in
funzione della probabilità $p$ si può notare come nei grafi ottenuti secondo il modello WS,
in corrispondenza di valori di $p$ appena più grandi di $0$, $L$ ha
un brusco calo rispetto al suo valore inziale, mentre $C$ si mantiene costante intorno al 
valore, tipicamente abbastanza alto, del grafo regolare di partenza: in definitiva, per i
valori di $p$ per cui si verifica quest'ultima configurazione, si ottiene un grafo
che gode della proprietà del piccolo mondo. In particolare, questo accade perché la riconnessione
degli archi a nuovi nodi genera archi che connettono nodi molto distanti fra loro: tali archi
sono detti infatti \textit{shortcuts}, in quanto accorciano le distanze tra i nodi tipiche
dei grafi regolari, lasciando tuttavia intatto il grado locale.

La rapida diminuizione del valore di $L$ e il mantenimento di valori quasi costanti di $C$
all'aumento di $p$ può essere giustificato dal fatto che la dipendenza di $L$ da $p$ non è
lineare: infatti, quando due nodi molto distanti sono connessi, l'abbreviazione del loro cammino
minimo non è risentita solo dai due nodi coinvolti, ma anche dai nodi a loro immediatamente
adiacenti. Tuttavia, la creazione di questi archi ha un effetto lineare su $C$: a seguito di 
una piccola variazione di $p$, la diminuizione di $C$ sarà altrettanto piccola.

\section{Modello di Barab\'{a}si e Albert}
Il modello di Barab\'{a}si Albert \cite{barabasi1999emergence, boccaletti2006complex}, indicato anche come 
modello BA, spiega la topologia delle reti ad invarianza di scala mettendo in relazione l'esistenza di una 
distribuzione del grado $P(k)$ a legge di potenza con la modalità in cui il sistema si è evoluto 
nel tempo. Il modello è stato originariamente pensato per descrivere la struttura del World Wide 
Web. L'idea di base del modello è che nel World Wide Web i siti web con grado elevato acquisiscono
nuovi collegamenti o punteggi più alti rispetto ai nodi con grado più basso.

Il modello BA consente di generare grafi aleatori, denotati come $G_{BA}=(V,E)$, caratterizzati
da una distribuzione di grado $P(k)$ a legge di potenza ottenibile mediante un processo particolare
di crescita denominato connessione preferenziale, secondo cui i nuovi nodi che sono aggiunti
alla rete si connettono a quelli preesistenti proporzionalmente al loro grado. Pertanto, 
maggiore è il grado dei nodi preesistenti, maggiore è la probabilità che i nuovi nodi si
connettano ad essi piuttosto che a nodi con grado inferiore.

La generazione dei grafi aleatori avviene secondo la seguente procedura:\linebreak inizialmente il grafo
è costituito da $n_0$ nodi isolati; successivamente, ad ogni istante $t=1,2,\ldots ,|V|-n_0$ 
è aggiunto un nuovo nodo $v_i$ con $m<n_0$ archi connessi ad esso. In questo caso la probabilità 
che un arco colleghi il nodo $v_j$ con un nodo $v_i$ già esistente sarà linearmente proporzionale 
al grado attuale del nodo $v_i$, ossia:

\begin{equation}
\prod_{v_j\rightarrow v_i}\frac{k_{v_i}}{\sum_{l}k_{v_l}}.
\label{eq2}
\end{equation}

In altre parole, l'equazione \eqref{eq2} denota la probabilità che un arco colleghi il 
nodo $v_j$ ad un nodo $v_i$ della rete e definisce la cosidetta \textit{connessione preferenziale}. La
somma totale dei gradi di tutti i nodi che compare al denominatore dell'equazione \eqref{eq2} è
un fattore di normalizzazione.

Nel modello di Barab\'{a}si Albert avremo dunque che all'istante di tempo $t$ il grafo $G$ 
avrà $m_0+t$ nodi e $mt$ archi. Il grado medio del grafo generato sarà $\langle k \rangle=2m$ 
ad istanti di tempo abbastanza grandi. 

Per grafi ottenuti tramite questo modello è stato stimato che il cammino caratteristico $L$ cresce
logaritmicamente rispetto a $|V|$ ed è più piccolo rispetto a quello di grafi ER con pari numero
di vertici e archi; il coefficiente di clustering $C$, invece descresce rispetto a $|V|$ secondo 
la legge $C\approx |V|^{-\frac{3}{4}}$, più lentamente rispetto ad un grafo ER con $C\approx |V|^{-1}$ 
\cite{albert2002statistical}.

In \figurename~\ref{fig:barabasiAlbert} è mostrato un grafo aleatorio costruito secondo
il modello di Barab\'{a}si Albert.

\begin{figure}[h]
\begin{center}
\includegraphics[width=14cm]{figures/barabasiAlbert}
\end{center}
\caption[Rete aleatoria generata secondo il modello di Barab\'{a}si Albert]
{Rete aleatoria generata secondo il modello di Barab\'{a}si Albert. I parametri iniziali
sono $n_0=100$ e $m_0=5$.}
\label{fig:barabasiAlbert}
\end{figure}

\begin{oss}

La distanza media dei nodi nel modello di Barab\'{a}si Albert è minore rispetto ad un grafo aletaorio di 
Erd\H{o}s e R\'{e}nyi con medesimi valori di $|V|$ e $|E|$.

\end{oss}

Esiste una variante del modello che produce grafi diretti, ossia il modello di Krapivsky-Redner
\cite{krapivsky2003dynamics}.
In questo modello si introduce il concetto di età $a_{v_i}$ di un nodo $v_i$ inteso come il numero
di nodi aggiunti dopo il nodo $v_i$. Sia, inoltre, $k_{v_i}$ il grado del nodo $v_i$ e $N_a(k)$
il numero di nodi di età $a$ aventi grado $k$. È possibile dimostrare che:

\begin{displaymath}
N_a(k)=\sqrt{1-\frac{a}{|V|}} \bigg(1-\sqrt{1-\frac{a}{|V|}}\bigg)^k.
\end{displaymath}

\section{Modello geometrico}
Il modello geometrico \cite{penrose2003random} è basato su una rappresentazione del grafo intesa
come una collezione di vertici aventi ciascuno una posizione specifica in uno spazio vettoriale
$\mathbb{R}^d$. I grafi geometrici possono essere descritti mediante uno specifico modello matematico.
In particolare, siano $\lVert\cdot\rVert$ una norma nello spazio vettoriale $\mathbb{R}^d$ e
$r$ una costante intera positiva. Dato un insieme finito di vertici $V\in\mathbb{R}^d$ si definisce 
grafo geometrico e lo si denota come $G_{Ge}(V; r)$, il grafo non orientato avente $V$ come
insieme dei vertici ed i cui archi collegano tutte le coppie $\{V,W\}$ tali che $\lVert W-V\rVert\leq 
r$. Se un grafo geometrico è caratterizzato da una configurazione casuale dei suoi vertici si definisce
aleatorio. In particolare sia $f$ una funzione di densità di probabilità su uno
spazio vettoriale $\mathbb{R}^d$ e $V_n=\{V_1,V_2,\ldots V_n\}$ un insieme di $n$ variabili 
$d$-dimensionali indipendenti distribuite con densità di probabilità $f$, allora il grafo
$G_{Ge}(V_n;r)$ sarà chiamato grafo aleatorio geometrico. 

In \figurename~\ref{fig:randomGeometricGraph}
è mostrato un esempio di grafo aleatorio geometrico in uno spazio bidimensionale con un numero
di nodi inziali $n_0=256$ e valore della costante $r=0.1$.

\begin{figure}[h]
\begin{center}
\includegraphics[width=16cm]{figures/randomGeometricGraph}
\end{center}
\caption[Rete aleatoria generata secondo il modello geometrico]
{Grafo aleatorio geometrico. Il grafo è rappresentato in uno spazio bidimensionale $[0,1]$ ($d=2$) e i 
parametri iniziali sono $n_0=256$ e $r=0.1$\protect\footnotemark.}
\label{fig:randomGeometricGraph}
\end{figure}
\footnotetext{Immagine tratta da Wikimedia Commons: 
\url{https://commons.wikimedia.org/wiki/File:Random\_geometric\_graph.svg}}

\section{Modello Forest-fire}
I modelli analizzati fino ad ora riproducono molte proprietà osservabili nelle reti reali,
ma non tutte: in particolare, è stato constatato che molte tra queste reti tendono a diventare dense nel tempo
e pertanto il numero dei loro archi tende quindi a crescere superlinearmente rispetto al numero di vertici. 
In altri casi è possibile riscontare che la distanza media tra i nodi si riduce progressivamente, piuttosto che 
crescere lentamente in funzione dell'aumento del numero di nodi, come piuttosto ci si aspetterebbe.

Per queste ragioni è stato introdotto un modello chiamato Forest-fire \cite{leskovec2005graphs} in grado 
di generare grafi aleatori, denotati come $G_{Ff}=(V,E)$, che durante la loro evoluzione temporale 
tendono a diventare densi e a ridurre progressivamente il valore del loro diametro. 

I passi per la creazione del grafo sono i seguenti: inizialmente
si hanno un grafo $G$ vuoto e una sequenza di $|V|$ vertici $v_1,v_2,\ldots, v_{|V|}$ non collegati
fra loro. All'istante $t>1$ il grafo conterrà $n_t$ vertici. L'algoritmo di generazione del grafo
aleatorio è ricorsivo: al primo passo viene selezionato un nodo $v_i$ dalla sequenza iniziale di vertici
e viene aggiunto alla rete; quindi dopo aver scelto casualmente un altro nodo $v_j$, detto ``ambasciatore'', 
$v_i$ viene collegato a $v_j$ tramite un arco. Successivamente, l'algoritmo genera un numero casuale 
$x$, con una distribuzione di probabilità binomiale avente media $(1-p)^{-1}$, dove $p$ è detta 
\textit{forward burning probability}. Il passo successivo consiste nell'aggiunta di 
esattamente $x$ archi in uscita collegati con altrettanti nodi presenti nel grafo e un numero di archi 
in ingresso con una probabilità $r$, detta \textit{backward burning ratio}, leggermente minore rispetto 
a $x$. Siano pertanto $w_1, w_2, \ldots, w_x$ i nodi destinazione degli archi appena aggiunti: l'algoritmo 
procederà ricorsivamente su di essi fino a quando i nodi da esaminare non saranno esauriti.

%Il modello Forest-fire descrive reti aleatorie che si evolvono nel tempo in cui il numero di archi
%cresce superlinearmente rispetto al numero di nodi e la distanza tra i vari nodi, all'aumentare di
%questi ultimi, si restringe progressivamente.

\section{Modello basato su duplicazione}
Le reti biologiche di grandi dimensioni spesso assumono comportamenti differenti rispetto ad altre
categorie di reti reali. In particolar modo è stato osservato che in diverse reti biologiche 
ad invarianza di scala, la distribuzione di probabilità del grado rispetta la legge di potenza
definita nell'Equazione \eqref{eq:scale-free} con un esponente $\beta$ compreso nell'intervallo 
$1<\beta <2$ \cite{aiello2000random}, diversamente da quanto ci si aspetterebbe: questo significa che 
tipicamente le reti biologiche sono soggette ad un'evoluzione differente rispetto ad altre reti ed in 
particolare il loro modello di evoluzione è caratterizzato da un processo di duplicazione dei nodi e 
delle connessioni tra di essi.

Per questo motivo è stato introdotto un modello di generazione di reti aleatorie basate su duplicazione 
\cite{chung2003duplication} che consente di generare reti denotate come $G_{Du}=(V,E)$ dove può essere 
applicato un meccanismo di duplicazione che coinvolge i nodi e le loro connessioni. Il processo 
di duplicazione, in particolare, può essere totale o parziale.

Il processo di duplicazione totale funziona nel seguente modo: sia $G_{Du}(t)=(V(t),E(t))$ il grafo aleatorio
all'istante $t>1$ di esecuzione dell'algoritmo e sia $v_i\in V(t)$ un suo vertice scelto casualmente con una 
certa probabilità $p$ fissata; un nuovo vertice $v_j$ viene aggiunto a $G_{Du}(t)$ in modo tale che per ogni 
nodo $v_k\in N(G_{Du}(t),v_i)$ è aggiunto un nuovo arco $(v_j,v_k)$ in $G_{Du}(t)$. Tipicamente la probabilità $p$ 
con cui è il nodo da duplicare viene scelto è uguale per ogni step dell'algoritmo. Se l'arco $(v_j,v_k)$ viene 
aggiunto a $G_{Du}(t)$ con una probabilità $q$ fissata, il processo di duplicazione è detto parziale. Il 
processo può quindi essere ripetuto iterativamente, per un numero di step fissato opportunamente, sul grafo
$G_{Du}(t+1)$ così ottenuto.

In \figurename~\ref{fig:duplication} è mostrato un esempio di duplicazione totale e parziale a partire
da un grafo iniziale contenente 7 vertici e 10 archi.

\begin{figure}[h]
\centering
\subfigure[]{%
\begin{tikzpicture}[scale = 0.5, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style_black} = [circle, draw = black, fill = black, text = white, 
		font=\footnotesize]
	\tikzstyle{node_style_grey} = [circle, draw = black, fill = {rgb,255:red,128; green,128; blue,128}, 
		semithick, text = white, font=\footnotesize]
	\tikzstyle{node_style_white} = [circle, draw = black, fill = white, semithick, text = black,
		font=\footnotesize]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\tikzstyle{label_node_style} = [draw = white, fill = white, font = \footnotesize]
	\node[label_node_style] (l3) at (3,2) {$v_i$};
	%\node[label_node_style] (l6) at (3,-2) {$v_j$};	
	\node[node_style_black] (v1) at (0,4) {};
	\node[node_style_black] (v2) at (-2,2) {};
	\node[node_style_white] (v3) at (2,2) {};
	\node[node_style_black] (v4) at (-2,0) {};
	\node[node_style_black] (v5) at (-2,-2) {};
	\node[node_style_black] (v6) at (2,-2) {};
	\node[node_style_black] (v7) at (0,-3) {};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v3) edge (v5);
	\draw[edge_style] (v3) edge (v7);
	\draw[edge_style] (v4) edge (v5);
	\draw[edge_style] (v4) edge (v6);
	\draw[edge_style] (v5) edge (v6);
\end{tikzpicture}}
\qquad
\qquad
\subfigure[]{%
\begin{tikzpicture}[scale = 0.5, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style_black} = [circle, draw = black, fill = black, text = white, 
		font=\footnotesize]
	\tikzstyle{node_style_grey} = [circle, draw = black, fill = {rgb,255:red,128; green,128; blue,128}, 
		semithick, text = white, font=\footnotesize]
	\tikzstyle{node_style_white} = [circle, draw = black, fill = white, semithick, text = black,
		font=\footnotesize]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\tikzstyle{label_node_style} = [draw = white, fill = white, font = \footnotesize]
	\node[label_node_style] (l3) at (3,2) {$v_i$};
	%\node[label_node_style] (l6) at (3,-2) {$v_j$};	
	\node[label_node_style] (l6) at (3,0) {$v_j$};	
	\node[node_style_black] (v1) at (0,4) {};
	\node[node_style_black] (v2) at (-2,2) {};
	\node[node_style_white] (v3) at (2,2) {};
	\node[node_style_black] (v4) at (-2,0) {};
	\node[node_style_black] (v5) at (-2,-2) {};
	\node[node_style_black] (v6) at (2,-2) {};
	\node[node_style_black] (v7) at (0,-3) {};
	\node[node_style_grey] (v8) at (2,0) {};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v3) edge (v5);
	\draw[edge_style] (v3) edge (v7);
	\draw[edge_style] (v4) edge (v5);
	\draw[edge_style] (v4) edge (v6);
	\draw[edge_style] (v5) edge (v6);
	\draw[edge_style] (v8) edge (v1);
	\draw[edge_style] (v8) edge (v2);
	\draw[edge_style] (v8) edge (v4);
	\draw[edge_style] (v8) edge (v5);
	\draw[edge_style] (v8) edge (v7);
\end{tikzpicture}}
\qquad
\qquad
\subfigure[]{%
\begin{tikzpicture}[scale = 0.5, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style_black} = [circle, draw = black, fill = black, text = white, 
		font=\footnotesize]
	\tikzstyle{node_style_grey} = [circle, draw = black, fill = {rgb,255:red,128; green,128; blue,128}, 
		semithick, text = white, font=\footnotesize]
	\tikzstyle{node_style_white} = [circle, draw = black, fill = white, semithick, text = black,
		font=\footnotesize]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\tikzstyle{label_node_style} = [draw = white, fill = white, font = \footnotesize]
	\node[label_node_style] (l3) at (3,2) {$v_i$};
	%\node[label_node_style] (l6) at (3,-2) {$v_j$};
	\node[label_node_style] (l6) at (3,0) {$v_j$};	
	\node[node_style_black] (v1) at (0,4) {};
	\node[node_style_black] (v2) at (-2,2) {};
	\node[node_style_white] (v3) at (2,2) {};
	\node[node_style_black] (v4) at (-2,0) {};
	\node[node_style_black] (v5) at (-2,-2) {};
	\node[node_style_black] (v6) at (2,-2) {};
	\node[node_style_black] (v7) at (0,-3) {};
	\node[node_style_grey] (v8) at (2,0) {};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v1) edge (v3);
	\draw[edge_style] (v1) edge (v4);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v3) edge (v5);
	\draw[edge_style] (v3) edge (v7);
	\draw[edge_style] (v4) edge (v5);
	\draw[edge_style] (v4) edge (v6);
	\draw[edge_style] (v5) edge (v6);
	\draw[edge_style] (v8) edge (v1);
	\draw[edge_style] (v8) edge (v2);
	\draw[edge_style] (v8) edge (v5);
\end{tikzpicture}}
\caption[Processo di duplicazione totale e parziale in un grafo.]{Processo di duplicazione totale e parziale
in un grafo: un nodo $v_i$ è selezionato con una probabilità $p$ dal grafo originale (a); il processo di 
duplicazione totale prevede l'aggiunta di un nuovo nodo $v_j$ collegato a tutti i vicini del nodo $v_i$ (b), 
mentre il processo di duplicazione parziale prevede che il vertice sia collegato a tutti i vicini di $v_i$ 
con una probabilità $q$ (c).}
\label{fig:duplication}
\end{figure}
