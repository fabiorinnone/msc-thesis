\chapter{Subgraph matching}
\vspace{4cm}

La funzionalità principale di NetMatch* consiste nella ricerca di tutte le di sottostrutture
di un grafo $G_q=(V_q,E_q)$, denominato query, all'interno di un grafo $G_t=(V_t,E_t)$, indicato 
generalmente come target. Il problema, noto come subgraph matching, sarà illustrato dettagliatamente
in questo capitolo. Inoltre, sarà presentato l'algoritmo adottato da NetMatch* per la sua risoluzione.

\section{Definizione del problema}
La risoluzione del problema del subgraph matching è particolarmente complessa: sono state
introdotte diverse tecniche e svariate euristiche che possono consentire di raggiungere l'obiettivo
in tempi ragionevoli. 

Formalmente è possibile definire il problema dell'isomorfismo tra sottografi nel seguente modo: 
siano $G_q=(V_q,E_q,\alpha_q,\beta_q)$ e $G_t=(V_t,E_t,\alpha_t,\beta_t)$ rispettivamente un 
grafo query e un grafo target entrambi etichettati (\figurename~\ref{queryTarget}).

\begin{figure}[!h]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 1.25, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, semithick]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\node[node_style] (v1) at (-2,2) {1};
	\node[node_style] (v2) at (-2,0) {2};
	\draw[edge_style] (v1) edge (v2);
	\end{tikzpicture}}
\qquad\qquad\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 1.25, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, semithick]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\node[node_style] (v1) at (-2,2) {1};
	\node[node_style] (v2) at (-2,0) {2};
	\node[node_style] (v3) at (0,0) {3};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v1) edge (v3);
	\end{tikzpicture}}
\caption[Grafo query $G_q$ e grafo target $G_t$]{Grafo query $G_q$ (a) e grafo target $G_t$ (b).}
\label{queryTarget}
\end{figure}

Sia inoltre
$\mu_q = (u_{q_1},u_{q_2},\ldots ,u_{q_{|V_q|}})$ un ordinamento dei vertici di $V_q$, tale che
$u_{q_i}=v_{q_j}\in V_q$ con $1\leq j\leq |V_q|$. Il problema dell'isomorfismo tra sottografi 
sarà dunque quello di ricercare tutte le possibili combinazioni dei $|V_p|$ vertici del grafo target 
$\mu_t = (u_{t_1},u_{t_2},\ldots ,u_{t_{|V_t|}})$ tali che $u_{t_i}=v_{t_j}\in V_t$ con 
$1\leq j\leq |V_t|$. 

La soluzione tipicamente adottata per la risoluzione del problema del subgraph matching consiste nel 
costruire un albero di ricerca denominato \textit{Space Search Tree} adibito alla rappresentazione 
dello spazio di ricerce delle soluzioni \cite{nilsson2014principles}. L'albero di ricerca è caratterizzato 
da una radice fittizia e ogni suo nodo è etichettato con una coppia $(v_{q_i},v_{t_i})$ con $v_{q_i}\in 
V_q$ e $v_{t_i}\in V_t$ che rappresenta un possibile match tra i due vertici (\figurename~\ref{fig:searchTree}).

\begin{figure}[!h]
\begin{center}
\begin{tikzpicture}[scale = 1.25, node distance=1cm]
\tikzstyle{node_style_normal} = [rectangle, draw=white, minimum width=1cm, minimum height=0.5cm,
	draw=white, text centered, font=\footnotesize]
\tikzstyle{node_style_bold} = [text = black, font=\footnotesize\bfseries]
\tikzstyle{edge_style_normal} = [draw = black, semithick]
\tikzstyle{edge_style_bold} = [draw = black, very thick]

\node (root) at (0,0) [node_style_bold] {root};
\node (n1) at (-4,-1) [node_style_bold] {(1,1)};
\node (n2) at (0,-1)[node_style_normal] {(1,2)};
\node (n3) at (4,-1) [node_style_normal] {(1,3)};
\node (n4) at (-5,-2) [node_style_bold] {(2,2)};
\node (n5) at (-3,-2) [node_style_normal] {(2,3)};
\node (n6) at (-1,-2) [node_style_normal] {(2,1)};
\node (n7) at (1,-2) [node_style_normal] {(2,3)};
\node (n8) at (3,-2) [node_style_normal] {(2,2)};
\node (n9) at (5,-2) [node_style_normal] {(2,3)};

\draw [edge_style_bold] (root) edge (n1);
\draw [edge_style_normal] (root) edge (n2);
\draw [edge_style_normal] (root) edge (n3);
\draw [edge_style_bold] (n1) edge (n4);
\draw [edge_style_normal] (n1) edge (n5);
\draw [edge_style_normal] (n2) edge (n6);
\draw [edge_style_normal] (n2) edge (n7);
\draw [edge_style_normal] (n3) edge (n8);
\draw [edge_style_normal] (n3) edge (n9);
\end{tikzpicture}
\end{center}
\caption[Albero di ricerca per l'algoritmo brute-force]
{Albero di ricerca per l'algoritmo brute-force relativo ai grafi della
\figurename~\ref{queryTarget}: in questo caso l'unico match possibile corrisponde al cammino
$((1,1),(2,2))$.}
\label{fig:searchTree}
\end{figure}

Una mappatura completa dei vertici corrisponderà ad un cammino da un figlio della radice ad una foglia 
dell'albero di ricerca e sarà indicata come $M=((v_{q_1},v_{t_1}),\allowbreak (v_{q_2},v_{t_2}),
\allowbreak\ldots\allowbreak ,(v_{q_{|V_q|}}, v_{t_{|V_t|}}))$, dove una coppia di vertici 
$(v_{q_i},v_{t_i})$ rappresenta un vertice del grafo query mappato con il corrispondente vertice 
del grafo target. Un match parziale sarà un insieme ordinato denotato come $M[i]=((v_{q_1},v_{t_1}),
(v_{q_2},v_{t_2}),\allowbreak\ldots ,\allowbreak(v_{q_i},v_{t_i}))$ contenente le coppie di 
vertici mappate al passo $i$-esimo dell'esecuzione dell'algoritmo e sarà corrispondente
ad un cammino un nodo figlio della radice ad un nodo interno dell'albero di ricerca. Gli insiemi ordinati
dei primi $i$ vertici mappati in $M[i]$ del grafo query e del grafo target saranno invece indicati 
rispettivamente come $M_q[i]=(v_{q_1},v_{q_2},\allowbreak\ldots ,\allowbreak v_{q_i})$ e $M_t[i]=(v_{t_1},
v_{t_2},\allowbreak\ldots ,\allowbreak v_{t_i})$.

Sia inoltre $v\in V_q$ un generico vertice del grafo query e $f(v)\in V_t$ il corrispondente vertice
mappato nel grafo target, dove $f$ è la funzione di mappatura tra vertici (Definizione 
\ref{def:subgraph_matching}): la compatibilità tra le etichette dei vertici è garantita 
dall'applicazione di una funzione di equivalenza $\alpha_q(v)\cong\alpha_t(f(v))$ definita opportunamente.

In definitiva, l'efficienza dell'algoritmo di matching dipende quindi dall'euristica adottata per 
attraversare l'albero di ricerca e di conseguenza dai vincoli applicati prima e durante il suo 
attraversamento \cite{bonnici2016variable}.

\section{Strategia di ricerca brute-force}
Una strategia banale di ricerca delle occorrenze di un grafo query all'interno di un grafo 
target è di tipo brute-force. Siano $G_q=(V_q,E_q,\alpha_q,\beta_q)$ e $G_t=(V_t,E_t,\alpha_t,\beta_t)$ 
rispettivamente un grafo query ed un grafo target entrambi etichettati. 

La strategia brute-force consiste nell'accoppiare ciascun nodo dei due grafi nel tentativo di 
trovare una struttura uguale in entrambi. Per raggiungere tale obiettivo, pertanto, si 
costruisce l'albero di ricerca al fine di testare esaustivamente tutte le possibili combinazioni, 
fin quando non è individuato il match possibile, se esiste.

Il limite dell'algoritmo basato su brute-force è dovuto al fatto che se il grafo target ha $n$ nodi
i match possibili sono $n!$, pertanto i tempi di esecuzione dell'algoritmo sono proibitivi: infatti
la crescita dell'albero di ricerca è esponenziale rispetto alla dimensione del grafo.

Tuttavia, tramite opportune euristiche, è possibile ridurre lo spazio di ricerca cercando
di escludere in anticipo le soluzioni che portano con certezza ad un fallimento. Una tecnica di
questo tipo, definita backtracking, è stata proposta da Ullmann in \cite{ullmann1976algorithm} e 
consiste nell'abbandonare soluzioni parziali in alcuni casi specifici. In particolare il backtracking
procede mappando inizialmente un numero arbitrario di coppie di nodi e, in un momento successivo,
è mappato l'insieme dei nodi direttamente raggiungibili dai nodi della coppia. Nel caso in cui la 
mappatura ha successo si procede ad oltranza, altrimenti si torna indietro di un passo e si prova 
un mapping differente. L'algoritmo termina quando tutti i nodi sono stati mappati oppure quando 
tutte le alternative per il primo nodo da matchare sono state provate e portano tutte ad un fallimento. 
L'operazione, pertanto, effettua una ``potatura''  dell'albero di ricerca, determinando una riduzione
dello spazio di ricerca.

Un'evoluzione possibile dell'algoritmo di Ullmann consiste nell'affiancare all'operazione
di backtracking anche un'operazione di partizionamento \cite{schmidt1976} che tenta di dividere 
i vertici del grafo in gruppi possibilmente corrispondenti. Se la lista delle corrispondenze possibili
diventa vuota per un nodo della query, allora nessun isomorfismo è possibile.

\section{L'algoritmo VF2}
L'algoritmo di ricerca implementato in NetMatch è il VF2 ed è descritto in \cite{cordella2004sub}:
esso fornisce una soluzione più efficiente al problema del subgraph matching rispetto all'algoritmo di 
Ullmann. 

L'algoritmo applica una strategia di ricerca in profondità ed è basato su backtracking e sulla tecnica 
del $k$-look-ahead che consente di prevedere in anticipo se una coppia dell'albero di ricerca 
ancora da esaminare per il mapping, non porti ad una soluzione finale valida e promettente dopo $k$ 
passi. Tale tecnica consente dunque di valutare un numero inferiore di possibili match fra nodi.

Siano $G_q=(V_q,E_q,\alpha_q,\beta_q)$ e $G_t=(V_t,E_t,\alpha_t,\beta_t)$ rispettivamente un 
grafo query e un grafo target entrambi etichettati. 
Sia inoltre $M[i]=((v_{p_1},v_{t_1}),(v_{p_2},v_{t_2}),\ldots ,\allowbreak(v_{p_i},v_{t_i}))$ la mappatura
parziale contenente tutte le coppie di vertici $v_{p_k}\in V_q$ e $v_{t_k}\in V_t$ con $0\leq k\leq i$ 
ottenute fino al passo $i$-esimo di esecuzione dell'algoritmo.

%Definiremo l'insieme $M(s)$ come il matching parziale dello stato $s$, ossia l'insieme corrente 
%delle coppie di nodi dei due grafi che sono corrispondenti:

%\begin{displaymath}
%M(s)=\{(n,m)\in V_q\times V_t:n\text{ ha un match con }m\text{ in }M(s)\}.
%\end{displaymath}

%$M(s)$ identifica dunque due sottografi: $G_q(s)$ e $G_t(s)$ che saranno indicati
%rispettivamente con $M_q(s)$ e $M_t(s)$.
%$M(s)$ identifica anche un sottoinsieme di $M$. Passiamo dallo stato $s$ allo stato
%$s'$ aggiungendo ai sottografi parziali due nodi $(m, n)$, uno di $G_q$ e uno di $G_t$.
%Formalmente si ha:

%\begin{displaymath}
%M(s')=M(s)\cup(m,n)\text{ con }m\in V_q\text{ e }n\in V_t.
%\end{displaymath}

%a cui sono passate la mappatura iniziale o precedente $M[i]$ e la coppia nodi da aggiungere ad essa.

%La funzione di fattibilità è definita come $F=(M[i],v_{q_{i+1}},v_{t_{i+1}})$, ha come scopo quello 
%di filtrare quante più ``strade'' possibili per cercare di arrivare il più velocemente alla soluzione. 
%In particolare, tale funzione è composta da una parte sintattica e una semantica:

%\begin{displaymath}
%F=F_{syn}\wedge F_{sem}
%\end{displaymath}

%dove $F_{syn}(i,v_{q_{i+1}},v_{t_{i+1}})$ dipende esclusivamente dalla struttura del grafo, mentre 
%$F_{sem}(i,v_{q_{i+1}},v_{t_{i+1}})$ dipende dai suoi attributi.

%Sia $G=(V,E)$ un grafo orientato e $v\in V$ un suo vertice: è possibile denotare l'insieme dei nodi
%predecessori di $v$ in $G$ come $\mbox{Pred}(G,v)$ e l'insieme dei nodi successori di $v$ in $G$ come
%$\mbox{Succ}(G,v)$.

%Le regole di fattibilità sintattiche previste dall'algoritmo sono 5.

% TODO: definire regole sintattiche
%\begin{displaymath}
%F_{syn}(i,v_{q_{i+1}},v_{t_{i+1}}) = R_{pred}\wedge R_{succ}\wedge R_{in}\wedge R_{out}\wedge R_{new}
%\end{displaymath}

%dove le prime due regole hanno il compito di verificare la consistenza della 
%soluzione parziale $M[i]$ e le rimanenti tre hanno il compito di effettuare il taglio
%dell'albero di decisione al fine di ridurre lo spazio di ricerca.

%Siano $G_q[V_q[i]]$ e $G_t[V_t[i]]$ i sottografi indotti rispettivamente da $G_q$ e
%$G_t$, dove $V_q[i]\in V_q$ e $V_t[i]\in V_t$ sono gli insiemi di vertici mappati
%in $M[i]$. 

Sia $p=(v_{q_{i+1}},v_{t_{i+1}})$ la successiva coppia valida da aggiungere alla mappatura parziale.
L'insieme $M[i+1]=M[i]\cup p$ costituirà la nuova mappatura al passo successivo di esecuzione
dell'algoritmo: qualora la mappatura ottenuta copra tutti i nodi del grafo query, avremo $M[i+1]=M$ e 
l'algoritmo terminerà. La scelta dell'eventuale coppia $p$ da aggiungere a $M[i]$ è effettuata mediante 
la definizione di un insieme, denotato come $P[i]$, contenente tutte le coppie candidate ad estendere 
la mappatura corrente.

Allo scopo di determinare le coppie di vertici $p$ candidate da includere in $P[i]$, si 
definiscono gli insiemi: 

\begin{align}
T^\text{out}_q[i]&=\{v\in V_q:\overrightarrow{(v',v)}\in E_q\,v\notin M_q[i],v'\in M_q[i]\}\label{eq1}\\
T^\text{out}_t[i]&=\{v\in V_t:\overrightarrow{(v',v)}\in E_t,v\notin M_t[i],v'\in M_t[i]\}\label{eq2}
\end{align}

contenenti rispettivamente i vertici del grafo query e del grafo target non inclusi nella 
mappatura parziale, che sono raggiungibili a partire da vertici mappati. Analogamente, è possibile 
definire gli insiemi:

\begin{align}
T^\text{in}_q[i]&=\{v\in V_q:\overrightarrow{(v,v')}\in E_q,v\notin M_q[i],v'\in M_q[i]\}\label{eq3}\\
T^\text{in}_t[i]&=\{v\in V_t:\overrightarrow{(v,v')}\in E_t,v\notin M_t[i],v'\in M_t[i]\}\label{eq4}
\end{align}
 
contenenti rispettivamente i vertici del grafo query e del grafo target non inclusi
nella mappatura parziale, da cui è possibile raggiungere vertici mappati.

L'insieme $P[i]$ sarà popolato iterativamente prelevando coppie di vertici $(v_{q_k},v_{t_k})$ 
rispettivamente da \eqref{eq1} e \eqref{eq2} fin quando uno tra i due insiemi non risulterà vuoto 
oppure prelevando i vertici allo stesso modo rispettivamente da \eqref{eq3} e \eqref{eq4}.

Per stabilire se aggiungere una nuova coppia di vertici $p$ all'insieme
$M[i]$ produce una mappatura consistente che non possa quindi precludere la possibilità
di ottenere una soluzione valida, si applicano alcune regole di fattibilità. In particolare, si
distinguono regole di tipo sintattico e regole di tipo semantico. 
Le regole di tipo sintattico dipendono esclusivamente dalla struttura del grafo, mentre quelle 
di tipo semantico dipendono dalle etichette dei suoi vertici ed archi.

%$T^\text{out}_q[i]$ e $T^\text{out}_t[i]$ contenenti nodi
%non presenti nella mappatura parziale che sono rispettivamente destinazioni in $G_q$ e $G_t$
%di archi di nodi contenuti in $G_q[V_q[i]]$ e $G_t[V_t[i]]$.

%la scelta delle coppie candidate si basa sui 
%``terminal sets'', cioè gli insiemi dei nodi, denominati $T_q[i]$ e $T_t[i]$, 
%direttamente connessi a $G_q[V_{q_i}]$ e $G_t[V_{t_i}]$. Le coppie sono scelte in modo 
%da assicurare che l'inconsistenza sia riconosciuta il più presto possibile. Le coppie 
%candidate sono generate in accordo ad un criterio di ordinamento in modo da evitare che 
%diversi cammini di ricerca diano uno stesso mapping parziale.

In particolare, l'algoritmo VF2 definisce due regole di di tipo sintattico che 
hanno il di compito verificare la consistenza di una mappatura parziale $M[i+1]$ ottenuta aggiungendo 
una coppia di vertici $p$ alla mappatura precedente $M[i]$. Formalmente, sia 
$p=(v_{q_{i+1}},v_{t_{i+1}})$ una coppia di vertici candidata ad essere aggiunta a $M[i]$. 
Devono valere devono le seguenti condizioni:

\begin{displaymath}
\begin{split}
(\forall v_q\in M_q[i]\cap N_\text{in}(G_q,v_{q_{i+1}})
\exists v_t\in N_\text{in}(G_t,v_{t_{i+1}}):(v_q,v_t)\in M[i])\land\\
(\forall v_t\in M_t[i]\cap N_\text{in}(G_t,v_{t_{i+1}})\exists v_q\in N_\text{in}
(G_q,v_{q_{i+1}}):(v_q,v_t)\in M[i])
\end{split}
\end{displaymath}

\begin{displaymath}
\begin{split}
(\forall v_q\in M_q[i]\cap N_\text{out}(G_q,v_{q_{i+1}})
\exists v_t\in N_\text{out}(G_t,v_{t_{i+1}}):(v_q,v_t)\in M[i])\land\\
(\forall v_t\in M_t[i]\cap N_\text{in}(G_t,v_{t_{i+1}})\exists v_q\in N_\text{out}
(G_q,v_{q_{i+1}}):(v_q,v_t)\in M[i])
\end{split}
\end{displaymath}

Sono definite ulteriori tre regole di tipo sintattico che hanno invece il compito di 
effettuare la potatura dell'albero di ricerca. Siano:

%\begin{align*}
%T_q[i]&=T_q^\text{in}[i]\cup T_q^\text{out}[i] & T_t[i]&=T_t^\text{in}[i]\cup T_t^\text{out}[i].
%\end{align*}

\begin{displaymath}
T_q[i]=T_q^\text{in}[i]\cup T_q^\text{out}[i]\text{ e }T_t[i]=T_t^\text{in}[i]\cup T_t^\text{out}[i].
\end{displaymath}

È possibile definire i seguenti insiemi:

%\begin{align*}
%S_q[i]&=V_q\backslash M_q[i]\backslash T_q[i] & S_t[i]&=V_t\backslash M_t[i]\backslash T_t[i].
%\end{align*}

\begin{displaymath}
S_q[i]=V_q\backslash M_q[i]\backslash T_q[i]\text{ e }S_t[i]=V_t\backslash M_t[i]\backslash T_t[i].
\end{displaymath}

Si definiscono quindi le seguenti regole di fattibilità che hanno il compito di effettuare 
entrambe un'operazione di $1$-look-ahead sull'albero di ricerca:

\begin{displaymath}
\begin{split}
(|N_\text{out}(G_q,v_{q_{i+1}})\cap T^\text{in}_q[i]|\geq 
|N_\text{out}(G_t,v_{t_{i+1}})\cap T^\text{in}[i]|)\land\\
(|N_\text{in}(G_q,v_{q_{i+1}})\cap T^\text{in}_q[i]|\geq 
|N_\text{out}(G_t,v_{t_{i+1}})\cap T^\text{in}[i]|);
\end{split}
\end{displaymath}

\begin{displaymath}
\begin{split}
(|N_\text{out}(G_q,v_{q_{i+1}})\cap T^\text{out}_q[i]|\geq 
|N_\text{out}(G_t,v_{t_{i+1}})\cap T^\text{out}[i]|)\land\\
(|N_\text{in}(G_q,v_{q_{i+1}})\cap T^\text{out}_q[i]|\geq 
|N_\text{out}(G_t,v_{t_{i+1}})\cap T^\text{out}[i]|).
\end{split}
\end{displaymath}

La seguente regola ha infine il compito di effettuare un'operazione di $2$-look-ahead
sull'albero di ricerca:

\begin{displaymath}
\begin{split}
(S_q[i]\cap N_\text{in}(G_q,v_{q_{i+1}}|\geq |S_t[i]\cap N_\text{in}(G_t,v_{t_{i+1}}|)\land\\
(S_q[i]\cap N_\text{out}(G_q,v_{q_{i+1}}|\geq |S_t[i]\cap N_\text{out}(G_t,v_{t_{i+1}}|).
\end{split}
\end{displaymath}

Le regole di tipo semantico hanno invece il compito di definire le modalità di ricerca
dei match nel caso in cui nodi e archi abbiano attributi, attraverso la definizione
di una funzione di equivalenza. La condizione che deve essere verificata è la seguente:

\begin{displaymath}
\begin{split}
v_{q_{i+1}}\cong &v_{t_{i+1}}\land
\forall(v_q,v_t)\in M[i],(v_{q_{i+1}},v_q)\Rightarrow (v_{q_{i+1}},v_q)\cong (v_{t_{i+1}},v_t)\land\\ 
&v_{q_{i+1}}\cong v_{t_{i+1}}\land\forall(v_q,v_t)\in M[i],(v_q,v_{q_{i+1}})\Rightarrow (v_q,v_{q_{i+1}}).
\end{split}
\end{displaymath}

In \figurename~\ref{vf2Algorithm} è mostrato il diagramma di flusso dell'algoritmo VF2,
mentre una sua implementazione ad alto livello è descritta in Algoritmo 
\ref{vf2-pseudocode}. Inizialmente la mappatura è un insieme vuoto. Successivamente, per ogni 
mappatura intermedia $M[i]$, l'algoritmo calcola l'insieme $P[i]$ delle coppie di nodi candidate 
ad essere aggiunte alla mappatura corrente. Quindi valuta se siano verificate le
regole di fattibilità per ogni coppia $p\in P[i]$: se l'esito è positivo, sarà calcolata la 
mappataura successiva $M[i+1]= M[i]\cup p$ e l'algoritmo verrà richiamato ricorsivamente su 
$M[i+1]$. 

\begin{figure}[!h]
\vspace{1cm} % WORKAROUND
\begin{center}
\begin{tikzpicture}[node distance=2cm]
%\node (empty) [empty] {};
\node (start) [startstop] {Inizio};
\node (pro1) [process, below of=start, yshift=-0.5cm] {$M[i=0]=\O$};
\node (dec1) [decision, below of=pro1, yshift=-1cm] {$M[i]$ copre tutti i nodi?};
\node (end) [startstop, right of=dec1, xshift=3cm] {Fine};
\node (pro2) [process, below of=dec1, yshift=-1cm] {Calcola l'insieme $P[i]$};
\node (pro3) [process, below of=pro2, yshift=-1cm] {Sia $p\in P[i]$};
\node (dec2) [decision, below of=pro3, yshift=-1cm] {$p$ è una coppia fattibile?};
\node (pro4) [process, below of=dec2, yshift=-1cm] {$M[i\mathbin{{+}{=}}1] = M[i]\cup p$};
\node (pro5) [process, right of=dec2, xshift=3cm] {$P[i]=P[i]\setminus p$};
\node (dec3) [decision, right of=pro3, xshift=3cm] {$P[i]=\O$?};

\draw [arrow] (start) -- (pro1);
\draw [arrow] (pro1) -- (dec1);
\draw [arrow] (dec1) -- node[anchor=south] {\footnotesize{Y}} (end);
\draw [arrow] (dec1) -- node[anchor=east] {\footnotesize{N}} (pro2);
\draw [arrow] (pro2) -- (pro3);
\draw [arrow] (pro3) -- (dec2);
\draw [arrow] (dec2) -- node[anchor=east] {\footnotesize{Y}} (pro4);
\draw [arrow] (dec2) -- node[anchor=south] {\footnotesize{N}} (pro5);
\draw [arrow] (pro5) -- (dec3);
\draw [arrow] (dec3) -- node[anchor=west] {\footnotesize{Y}} (end);
\draw [arrow] (dec3) -- node[anchor=south] {\footnotesize{N}} (pro3);
%\draw [arrow] (pro5) |- (pro3);
\draw [arrow] (pro4.west) -| ++(-3cm,0) |- (dec1.west);
%\draw [arrow] (pro4) -| (pro5);
%\draw [arrow] (pro5) |- (pro3);
\end{tikzpicture}
\end{center}
\caption[Diagramma di flusso dell'algoritmo VF2]
{Diagramma di flusso dell'algoritmo VF2.}
\label{vf2Algorithm}
\end{figure}

\begin{algorithm}{VF2-MATCH($M[i]$)}
\caption{Algoritmo di matching VF2}
\label{vf2-pseudocode}
\begin{algorithmic}[1]
\REQUIRE{una mappatura intermedia $M[i]$. La mappatura iniziale è $M[0] = \O$}
\ENSURE{la mappatura completa $M$}
\IF{$M[i]$ copre tutti i nodi di $G_t$}
\STATE {\textbf{print} $M[i]$}
\ELSE
\STATE{calcola l'insieme $P[i]$ delle coppie candidate per l'inclusione in $M[i]$}
\FORALL{$p \in P[i]$}
\IF{$p$ è una coppia fattibile}
\STATE{$M[i+1] = M[i]\cup p$}
\STATE{VF2-MATCH($M[i+1]$)}
\ENDIF
\ENDFOR
%\STATE{aggiorna le strutture dati}
\ENDIF
\end{algorithmic}
\end{algorithm}

\section{L'algoritmo RI}
NetMatch* adotta RI \cite{bonnici2013subgraph} come algoritmo di subgraph matching. RI pone le sue 
basi dall'osservazione che l'ordine con cui i vertici del grafo target sono esaminati è 
un aspetto cruciale al fine di rendere più veloce la ricerca dei match: per esempio, la strategia di 
ricerca adottatta potrebbe prevedere la scelta di un nodo iniziale nel grafo query avente grado
massimo, oppure avente l'etichetta meno frequente nel\linebreak grafo target. L'algoritmo 
RI effettua dunque un ordinamento dei vertici del grafo query indipendente dalle proprietà del grafo 
target e mantiene il medesimo ordinamento per tutti i rami dell'albero di ricerca. In questo
caso si dirà che l'algoritmo di ricerca è di tipo statico \cite{ullmann2010bit}.

Siano $G_q=(V_q,E_q,\alpha_q,\beta_q)$ e $G_t=(V_t,E_t,\alpha_t,\beta_t)$ rispettivamente
 un grafo query e target entrambi etichettati: RI costruisce
un albero di ricerca in cui ogni ogni nodo rappresenta un possibile match tra un vertice 
$v_{q_i}\in G_q$ ed un vertice $v_{t_j}\in G_t$. 

Sia $p_t = ((v_{q_0},M(v_{q_0})), (v_{q_1},M(v_{q_1})),\allowbreak\ldots ,(v_{q_n},M(v_{q_n}))$ 
un cammino nell'albero di ricerca a partire da un nodo figlio della radice. Se $n<|V_q|$ allora $p_t$ 
sarà un match parziale tra $G_q$ e $G_t$, altrimenti se $n=|V_q|$ allora $p_t$ sarà una soluzione finale.

L'algoritmo RI, prima di avviare l'operazione di ricerca dell'isomorfismo, effettua un ordinamento
dei vertici del grafo query allo scopo di massimizzare le probabilità che un ramo dell'albero
di ricerca che conduce ad una soluzione fallimentare possa essere escluso. Si consideri il grafo
query $G_q=(V_q,E_q,\alpha_q,\beta_q)$ e si supponga $n=|V_q|$. Occorrerà definire opportunamente una sequenza
di vertici $\mu_q =(v_{q_0},v_{q_1},\ldots ,v_{q_n})$ di $V_q$. In particolare, ad ogni passo
$i$, il vertice $v_{q_i}\in V_q$ sarà scelto in modo tale che massimizzi la dimensione
dell'insieme $B_i=\{(v_{q_i},v_{q_j})\in E_q: v_{q_j}\in\mu ,0<j\leq i\leq n\}$, ossia
l'insieme degli archi del grafo query che collegano $v_{q_i}$ con i vertici in $\mu_q$. Rendendo
l'insieme $B_i$ più grande possibile, l'algoritmo impone molti vincoli sui corrispondenti
sottografi del grafo target. In questo modo, RI troverà inizialmente dei match tra nodi
altamente connessi con nodi per i quali sono già stati individuati dei match.

Per individuare una buona sequenza di vertici $\mu_q$ RI utilizza un algoritmo greedy chiamato
GreatestConstraintFirst. In particolare, l'algoritmo visita i vertici del grafo query 
in base ad una apposita funzione che assegna un punteggio a ciascun vertice: inizialmente l'algoritmo
esamina un vertice $v_{q_0}$ tale che abbia il massimo numero di vicini rispetto agli altri vertici 
del grafo; successivamente procede iterativamente fin quando tutti i vertici sono stati inseriti 
in $\mu_q$.

Sia $N(G_q,v_{q_i})$, con $v_{q_i}\in V_q$ e $0<i<|V_q|$, l'insieme dei vicini del nodo $v_{q_i}$ 
nel grafo $G_q$. I punteggi sono calcolati nel seguente modo: sia $i$ il successivo passo nella 
visita dei vertici del grafo query e sia $\mu_q[i]=(v_0,v_1,\ldots ,v_{i-1})$ l'ordinamento
parziale dei vertici fino a quel momento. Sia inoltre $v_{q_k}\in N(\mu_q[i])$ il vertice candidato 
come successivo ad essere inserito in $\mu_q$. L'algoritmo assegnerà un punteggio a $v_{q_k}$ sulla 
base della definizione dei seguenti tre insiemi di vertici vicini:

\begin{enumerate}

\item $N_1(G_q,v_{q_k})=\{v_{q_j}:v_{q_j}\in N(G_q,v_{q_k})$ e $v_{q_j}\in \mu_q[i]\}$ che rappresenta l'insieme
dei vertici in $\mu$ che sono vicini di $v_{p_k}$ e che sono inclusi nell'ordinamento parziale;

\item $N_2(G_q,v_{q_k})=\{v_{q_j}:v_{q_j}\in N(G_q,v_{q_k})$ e $v_{q_j}\in\{N(\mu_q[i])\}\}$ che
rappresenta l'insieme dei vertici che sono vicini di almeno un vertice compreso in $\mu_p[i]$;

\item $N_3(G_q,v_{q_k})=\{v_{q_j}:v_{q_j}\in N(G_q,v_{q_k})$ e $v_{q_j}\notin\{N_1(G_q,v_{q_k})\cup 
N_2(G_q,v_{q_k})\}\}$ che rappresenta l'insieme dei vertici che non inclusi in $\mu_q$, che non sono vicini 
di vertici in $\mu_q$, ma che sono vicini del vertice $v_{q_k}$.

\end{enumerate} 

\newpage % workaround issue #89
Tale strategia di ordinamento, adottata da RI, è denominata 3-way-N (\figurename~\ref{riStrategy}).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=13cm]{figures/threeWayNeighboroodSets.png}
\end{center}
\caption[Strategia 3-way-N adottata da RI]
{Strategia 3-way-N adottata da RI: il vertice di colore verde è $v_{p_k}$; i vertici di colore rosso
appartengono all'insieme $N_1(v_{p_k})$; i vertici di colore giallo appartengono all'insieme $N_2(v_{p_k})$;
infine i vertici di colore blu appartengono all'insieme $N_3(v_{p_k})$.}
\label{vf2Algorithm}
\end{figure}

In Algoritmo \ref{gcf-pseudocode} è mostrato lo pseudocodice di GreatestConstraintFirst.

\afterpage{%
	\begin{algorithm}{GREATEST\_CONSTRAINT\_FIRST}
	\caption{Algoritmo GreatestConstraintFirst}
	\label{gcf-pseudocode}
	\begin{algorithmic}[1]
	\REQUIRE{un grafo query etichettato $G_q=(V_q,E_q,\alpha_q,\beta_q)$, $n=|V_q|$\\
	un grafo target etichettato $G_t=(V_t,E_t,\alpha_t,\beta_t)$}
	\ENSURE{la lista ordinata dei vertici query $\mu_q=(v_{q_0},v_{q_1},\ldots ,v_{q_n})$\\
	la lista ordinata dei genitori dei vertici query 
	$\pi_\mu=(\pi(v_{q_0}),\pi(v_{q_1}),\ldots ,$ $\pi(v_{q_n}))$}
	\STATE{sia $v_{q_0}$ il vertice con il grado massimo}
	\STATE{$V_q=V_q\backslash\{v_{q_0}\}$}
	\STATE{$\mu_q=(v_{q_0})$}
	\STATE{$\pi(v_{q_0})=\text{null}$}
	\STATE{$\pi_{\mu_q}=\pi(v_{q_0})$}
	\STATE{$v_{q_m}=\text{null}$}
	\STATE{$\text{rank} = (-\infty,-\infty,-\infty)$}
	\WHILE{$V_q\neq \O$}
	\STATE{$m=|\mu_q|$}
	\FORALL{$v_{p_k}\in V_q$}
	\STATE{$N_1(G_q,v_{q_k})=\{v_{q_j}:v_{q_j}\in N(G_q,v_{q_k})$ e $v_{q_j}\in \mu_q[i]\}$}
	\STATE{$N_2(G_q,v_{q_k})=\{v_{q_j}:v_{q_j}\in N(G_q,v_{q_k})$ e $v_{q_j}\in\{N(G_q,\mu_q[i])\}\}$}
	\STATE{$N_3(G_q,v_{q_k})=\{v_{q_j}:v_{q_j}\in N(G_q,v_{q_k})$ e $v_{q_j}\notin\{N_1(G_q,v_{q_k})\cup N_2(G_q,v_{q_k})\}\}$}
	\IF{$\text{rank}\leq (|N_1(G_q,v_{q_k})|,|N_2(G_q,v_{q_k})|,|N_3(G_q,v_{q_k})|)$}
	\STATE{$v_{q_m} = v$}
	\STATE{$\text{rank}=(|N_1(G_q,v_{q_k})|,|N_2(G_q,v_{q_k})|,|N_3(G_q,v_{q_k})|)$}
	\ENDIF
	\ENDFOR
	\STATE{sia $v_{q_i}\in\mu_q$ tale che $i$ è il minimo indice per cui esiste $(v_{q_m},v_{q_i})\in E_q$}
	\STATE{$\pi(v_{q_m})=\pi(v_{q_i})$} %TODO verificare se questa istruzione è corretta
	\STATE{\texttt{append} $v_m$ a $\mu_q$}
	\STATE{\texttt{append} $\pi(v_{q_m})$ a $\pi_{\mu_q}$}
	\STATE{$V_q=V_q\backslash \{v_{q_m}\}$}
	\ENDWHILE
	\end{algorithmic}
	\end{algorithm}
}

Definita l'ordinamento iniziale dei vertici del grafo pattern e individuata la strategia di
ricerca, il passo successivo è ridurre opportunamente lo spazio di ricerca. Ogni nodo dell'albero
di ricerca rappresenta una mappatura da un vertice del grafo query ad un vertice
del grafo target: i vertici del grafo pattern devono essere quindi confrontati con tutti i vertici
del grafo target per verificare quali di essi soddisfano le condizioni di isomorfismo.
Ad ogni passo $i$, in particolare, occorre scegliere i vertici candidati nel grafo target 
$v_{t_i} = M(v_{q_i})$, tra i vicini dei vertici che hanno un match con i nodi genitori, ottenuti
tramite l'algoritmo GreatestConstraintFirst. Sia $M(i)=(v_{p_k},v_{t_j})$ l'$i$-esima
coppia di un match parziale: ad ogni $i$-esimo passo della sua esecuzione, RI farà ricorso 
alle seguenti condizioni di isomorfismo:

\begin{enumerate}

\item $v_{t_j}$ non deve essere incluso nel match parziale corrente;

\item deve valere $|N(V_q,v_{q_k})|\leq |N(G_t,v_{t_j})|$;

\item i vertici devono essere compatibili, quindi deve valere $\alpha_q(v_{p_k})=\alpha_t(v_{t_j})$,
dove $\alpha_q:V_q\rightarrow\Sigma$ e $\alpha_t:V_t\rightarrow\Sigma$ sono rispettivamente le funzioni 
di etichettatura dei nodi dei grafi query e target;

\item devono essere verificati i vincoli sulle etichette degli archi: per ogni arco $(v_{p_k},
M(v'_p))\in E_p:v'_p\in\mu_p[i-1]$ deve valere $(v_{t_j},M(v'_p))\notin E_t$ e $\beta_p((v_{p_k},
v'_p))=\beta_t((v{t_j},M(v'_p))\notin E_t$, dove $\beta_p:E_p\rightarrow\Sigma$ e
$\beta_t:E_p\rightarrow\Sigma$ sono rispettivamente le funzioni di etichettatura degli archi dei
grafi query e target; 

\item per ogni arco $(v_{p_k},v'_p)\notin E_p$ si deve avere $(v_{p_j},M(v'_p))\notin E_t$.
\end{enumerate}  

La strategia di ricerca adottata da RI è descritta in \figurename~\ref{riStrategy}. 

\begin{figure}[!h]
\centering
\begin{tikzpicture}[scale = 1.25, shorten >= 1pt, auto, node distance = 3cm, semithick]
\tikzstyle{black_node_style} = [circle, draw = black, fill = white, text = black, semithick]
\tikzstyle{red_node_style} = [circle, draw = red, fill = white, text = red, semithick]
\tikzstyle{black_edge_style} = [draw = black, semithick]
\tikzstyle{red_edge_style} = [draw = red, semithick]
\tikzstyle{grey_edge_style} = [draw = {rgb,255:red,128; green,128; blue,128}, semithick]
\node[black_node_style] (v0) at (-2,4) {0};
\node[red_node_style] (v1) at (-2,2) {1};
\node[black_node_style] (v2) at (-2,0) {2};
\node[black_node_style] (v3) at (0,4) {3};
\node[red_node_style] (v4) at (0,2) {4};
\node[black_node_style] (v5) at (0,0) {5};
\node[black_node_style] (v6) at (2,4) {6};
\node[black_node_style] (v7) at (2,2) {7};
\node[black_node_style] (v8) at (2,0) {8};
\node[black_node_style] (v9) at (0,6) {9};
\draw[red_edge_style] (v0) edge (v1);
\draw[red_edge_style] (v0) edge (v3);
\draw[red_edge_style] (v0) edge (v4);
\draw[black_edge_style] (v0) edge (v9);
\draw[red_edge_style] (v1) edge (v2);
\draw[black_edge_style] (v1) edge (v4);
\draw[red_edge_style] (v1) edge (v5);
\draw[grey_edge_style] (v2) edge (v5);
\draw[black_edge_style] (v3) edge (v6);
\draw[red_edge_style] (v4) edge (v5);
\draw[red_edge_style] (v4) edge (v6);
\draw[red_edge_style] (v4) edge (v7);
\draw[grey_edge_style] (v5) edge (v7);
\draw[black_edge_style] (v5) edge (v8);
\draw[black_edge_style] (v6) edge (v7);
\draw[black_edge_style] (v6) edge (v9);
\draw[black_edge_style] (v7) edge (v8);
\end{tikzpicture}
\caption[Strategia di ricerca dell'algoritmo RI]
{Strategia di ricerca dell'algoritmo RI: il primo vertice inserito in $\mu_q$ è 4, in quanto ha 
il numero più elevato di archi. Successivamente, si supponga che $\mu_q=(4,1)$: in questo caso i 
possibili candidati da inserire nell'insieme sono i vertici 0, 2, 6, 5 e 7. Il vertice successivo
che viene inserito è 5 poiché tale vertice ha il numero più elevato di archi che puntano ai
rimanenti vertici di $\mu_q$.}
\label{riStrategy}
\end{figure}

I passi della procedura di matching dell'algoritmo RI sono descritti in dettaglio in Algoritmo 
\ref{ri-pseudocode}.

\afterpage{
	\begin{algorithm}{RI-MATCH}
	\caption{Algoritmo di matching RI}
	\label{ri-pseudocode}
	\begin{algorithmic}[1]
	\REQUIRE{un grafo query etichettato $G_q=(V_q,E_q,\alpha_q,\beta_q)$, $n=|V_q|$\\
	un grafo target etichettato $G_t=(V_t,E_t,\alpha_t,\beta_t)$\\
	la lista ordinata dei vertici query $\mu=(v_{q_0},v_{q_1},\ldots ,v_{q_n})$\\
	la lista ordinata dei genitori dei vertici query 
	$\pi_\mu=(\pi(v_{q_0}),\pi(v_{q_1}),\ldots ,\pi(v_{q_n}))$\\
	le condizioni di isomorfismo}
	\ENSURE{l'insieme contenente tutti i match $M_i=((v_{q_0},v_{t_0}),(v_{q_1},v_{t_1}),\ldots ,\{v_{q_n},t_{v_n}))$
	con $v_{q_i}\in G_q$ e $v_{t_i}\in G_t,0\leq i\leq n$}
	\FORALL{cammino $p_j$ nell'albero di ricerca}
	\FOR{$i=0$ to $n$}
	\STATE{sia $x\in V',x\neq p_j$ e $x$ un vicino del vertice $M(\pi(u_i))$ in $V'$}
	\IF{$x$ soddisfa le condizioni di isomorfismo}
	\STATE{\texttt{add} $(u_i,x)$ in $p_j$}
	\ENDIF
	\IF{$\nexists\mbox{ }x$}
	\STATE{\texttt{discard} $p_j$}
	\ENDIF
	\ENDFOR
	\IF{$i=n$}
	\STATE{\texttt{add} $p_j$ all'insieme $\{M_i\}$}
	\ENDIF
	\ENDFOR
	\RETURN l'insieme $\{M_i\}$
	\end{algorithmic}
	\end{algorithm}
}

Test empirici hanno dimostrato che l'algoritmo RI è più efficiente rispetto all'algoritmo
VF2 sia in grafi a media che ad alta densità \cite{carletti2013performance}. 

\section{L'algoritmo RI-DS}
L'algoritmo RI è stato pensato per essere efficiente sia in termini di velocità di esecuzione
che di memoria impiegata. Tuttavia, in alcuni casi specifici è possibile ottenere risultati
migliori in termini di tempi di esecuzione effettuando il calcolo di un'apposita mappa di
compatibilità tra i vertici del grafo query e quelli del grafo target. Questa tecnica è alla
base di una variante dell'algoritmo RI, denominata RI-DS \cite{bonnici2013subgraph}, 
che viene effettuata subito dopo la prima fase di definizione dell'ordinamento dei vertici dei 
grafi query e target e immediatamente prima che l'algoritmo di subgraph matching sia eseguito. 
Nello specifico, essa consiste nella definizione di un dominio di assegnamento inziale dove, 
per ogni vertice del grafo query, l'algoritmo calcola il suo dominio e verifica che gli archi 
del grafo query siano compatibili rispetto ai domini del grafo target. Questa fase preliminare 
di verifica si rivela particolarmente utile quando il grafo target è denso, in quanto consente
di ridurre sostanzialmente il numero di vertici candidati durante la fase successiva di 
backtracking. 

Nella versione standard dell'algoritmo è possibile che una coppia di un match $(v_{p_i},v_{t_i})$
possa essere verificata più volte nel corso dell'esecuzione dell'algoritmo: in questo caso RI
effettuerà altrettante volte anche la verifica della compatibilità delle etichette, che può
rilevarsi costosa da un punto di vista computazionale. A differenza di RI, pertanto, RI-DS 
verifica la compatibilità delle etichette solamente una volta e memorizza i risultati in una
apposita mappa di compatibilità $C$. In particolare, una entry della mappa $C[i][j]$ avrà valore 
true se $\alpha_q(v_{p_i})\cong\alpha_t(v_{t_i})$ e se $|N(G_q,v_{p_i})|<|N(G_t,v_{t_i})|$. Inoltre, RI-DS
utilizza una strategia di ordinamento differente rispetto a RI, denominata 3-way-N+D, che combina
le caratteristiche della strategia di ordinamento di RI con una strategia di ordinamento
a singolo dominio, dove le variabili con una cardinalità del dominio pari a $1$ sono inserite
nella prima posizione dell'ordinamento.
