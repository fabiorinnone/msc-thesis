\chapter{Risultati}
\vspace{4cm}

In questo capitolo saranno presentati i risultati comparativi delle prestazioni di NetMatch*
rispetto al plugin NetMatch e degli algoritmi RI e RI-DS. Inoltre saranno mostrati le misure
dei tempi necessari per la generazione delle reti random, per la verifica della significatività e per
il calcolo delle misure statistiche.

\section{Dataset utilizzati}
Per effettuare la comparazione dei tempi di esecuzione tra NetMatch e NetMatch* sono stati utilizzati
differenti dataset biologici. 

Un dataset utilizzato è stato quello relativo alle reti di interazione proteina-proteina contenente 10 reti 
biologiche che descrivono le interazioni tra proteine del database STRING \cite{szklarczyk2011string}. 
Nel dataset le reti relative ai seguenti organismi: \emph{Mus musculus}, \emph{Saccaromyces 
cerevisiae}, \emph{Caenorhabditis elegans}, \emph{Drosophila melanogaster}, \emph{Takifugu rubipres}, 
\emph{Danio rerio}, \emph{Xenopus tropicalis}, \emph{Bos taurus}, \emph{Rattus norvegicus} e\emph{Homo
sapiens}. I grafi del dataset originale sono tutti di grandi dimensioni e densi e ogni vertice ha una 
etichetta unica. Per effettuare la comparazione delle app, le etichette dei grafi sono state inserite 
casualmente, utilizzando rispettivamente 32, 64, 128, 512 e 2048 etichette sintetiche e 43 etichette 
reali corrispondenti alla classe della Gene Ontology delle proteine. Sono state usate come query delle 
sottoreti estratte dalle reti target, variando di volta in volta il numero di nodi tra i valori 4, 8, 16, 
32, 64 e modificando la densità da bassa ad alta: in particolare, il massimo valore di numero di archi 
delle reti query raggiunge il 90\% dei nodi.

Un altro dataset utizzato è stato quello relativo alle proteine back-bones denominato PDBSv2 
\cite{huehne2009jena,protein}. I grafi contenuti in questo dataset sono di media dimensione e sparsi: 
il numero di nodi varia da 1683 a 7979 vertici per grafo. I grafi query sono stati estratti dai 
corrispondenti grafi target variando il numero di archi tra i valori 4, 8, 16, 32, 64, 128 e 256.

Il terzo dataset adottato è infine quello relativo alle mappe di contatto di proteine denominato PDBSv3 
contenente 50 grafi densi, semidensi e sparsi. Anche in questo caso i grafi query sono stati ottenuti a 
partire dai grafi target variando il numero di archi nello stesso range del dataset precedente. 

\section{Comparazione dei tempi di esecuzione}

In \figurename~\ref{chartsPPI-4} è mostrato un grafico di confronto delle prestazioni di
NetMatch* rispetto a NetMatch su tre reti di interazione proteina-proteina prelevate dal database STRING: 
\emph{Mus musculus}, \emph{Homo sapiens} e \emph{Danio rerio}. Le reti esaminate sono dei grafi densi 
di grandi dimensioni. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/chartsPPI-4.png}
\end{center}
\caption[Tempi di esecuzione su reti di interazione proteina-proteina]
{Tempi di esecuzione su reti di interazione proteina-proteina.}
\label{chartsPPI-4}
\end{figure}

In \figurename~\ref{chartProteinBackbones} sono mostrati i risultati comparativi tra NetMatch* e
NetMatch su reti di proteine back-bones. In questo caso le reti target sono grafi sparsi di grandi 
dimensioni.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/chartProteinBackbones.png}
\end{center}
\caption[Tempi di esecuzione su proteine 3D]
{Tempi di esecuzione su proteine 3D.}
\label{chartProteinBackbones}
\end{figure}

In \figurename~\ref{chartProteinContactMaps} sono mostrati i risultati comparativi tra NetMatch*
e NetMatch su grafi di mappe di contatto di proteine. In questo caso le reti target sono grafi mediamente
densi. Sono state mantenute le etichette originali, tranne nei casi in cui non fossero uniche
(ad esempio nel caso degli aminoacidi).

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/chartProteinContactMaps.png}
\end{center}
\caption[Tempi di esecuzione su mappe di contatto di proteine]
{Tempi di esecuzione su mappe di contatto di proteine.}
\label{chartProteinContactMaps}
\end{figure}

In \figurename~\ref{9ldbChart} sono mostrati i tempi di esecuzione rispettivamente degli algoritmi
RI e RI-DS su una rete di interazione descrivente il comportamento della glicosidasi \cite{heinz1993amino}: 
gli algoritmi sono stati testati su insiemi di 10 query aventi rispettivamente 4, 8, 16 e 32 nodi.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=14cm]{figures/chart_9ldb.png}
\end{center}
\caption[Tempi di esecuzione di RI e RI-DS.]
{Tempi di esecuzione di RI e RI-DS.}
\label{9ldbChart}
\end{figure}

In \figurename~\ref{feedForwardLoop} sono mostrati i tempi di esecuzione della ricerca
del feed-forward loop sulla rete \emph{Mus musculus} con 512 etichette. Le query sono sia
di tipo esatto che approssimato. Nel caso di query approssimate possono avere uno, due o 
tutti i nodi senza etichetta, oppure un arco sostituito con un cammino la cui lunghezza
può variare da 3 a 7 archi.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=13cm]{figures/feedForwardLoop.png}
\end{center}
\caption[Tempi di esecuzione di query approssimate con feed-forward loop]
{Tempi di esecuzione di query approssimate con feed-forward loop: le query sono
eseguite su \emph{Mus musculus} con 512 etichette.}
\label{feedForwardLoop}
\end{figure}

In \figurename~\ref{models} sono mostrati i tempi di esecuzione relativi alla generazione
delle reti random secondo tutti i modelli di randomizzazione e i tempi medi di ricerca su esse
per alcune query. 

\begin{figure}[!h]
\begin{center}
\includegraphics[width=13cm]{figures/models.png}
\end{center}
\caption[Tempi di esecuzione della generazione e ricerca nelle reti random]
{Tempi di esecuzione della generazione e ricerca nelle reti random: le reti random
sono generate secondo i modelli Shuffling (Sh), Erd\H{o}s-R\'{e}nyi (ER), Watts-Strogats (WS),
Barab\'{a}si-Albert (BA), Geometric (Ge) Forest-fire (Ff) e Duplication (D) sulla rete 
\emph{Mus musculus}. La ricerca è stata effettuata usando come query il feed-forward loop.}
\label{models}
\end{figure}

In \figurename~\ref{zScore} sono mostrati, infine, i tempi medi per il calcolo della significatività
statistica (Z-score) per alcune query, su tutti i modelli di randomizzazione implementati
in NetMatch*.

\begin{figure}[!h]
\begin{center}
\includegraphics[width=13cm]{figures/zScore.png}
\end{center}
\caption[Valori dello Z-score per il feed-forward loop su \emph{Mus musculus}]
{Valori dello Z-score per il feed-forward loop su \emph{Mus musculus}: le reti random
sono senza etichette e generate secondo i modelli Shuffling (Sh), Erd\H{o}s-R\'{e}nyi (ER), 
Watts-Strogats (WS), Barab\'{a}si-Albert (BA), Geometric (Ge) Forest-fire (Ff) e Duplication (D).}
\label{zScore}
\end{figure}
