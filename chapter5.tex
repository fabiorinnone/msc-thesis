\chapter{Implementazione}
\vspace{4cm}

%In questo capitolo saranno presentati i dettagli implementativi di NetMatch*.

NetMatch* è stato sviluppato in linguaggio Java 7 facendo ricorso alle API di Cytoscape v3.4.0.
Il software è composto da un modulo principale, che implementa gli algoritmi e le strutture
dati e da una interfaccia utente che integra la componente algoritmica con l'interfaccia di Cytoscape.
Il codice sorgente \cite{rinnone2016datasource} di NetMatch* è rilasciato in MIT License. 

\section{Modulo principale}
Il modulo principale del software implementa le strutture dati e la componente algoritmica 
dell'applicazione. Le classi Java che gestiscono questa componente sono inglobate in due
specifici packages denominati \texttt{algorithm} (\figurename~\ref{algorithm_class_diagram}) e
\texttt{graph} (\figurename~\ref{graph_class_diagram}).

%Il package comprende le classi Java necessarie all'implementazione innanzitutto delle strutture
%dati

\begin{figure}[H]
\begin{center}
\includegraphics[width=14cm]{figures/algorithm_class_diagram.png}
\end{center}
\caption[Diagramma delle classi del package \texttt{algorithm}]{Diagramma delle classi del 
package \texttt{algorithm}.}
\label{algorithm_class_diagram}
\end{figure}

\begin{figure}[H]
\begin{center}
\includegraphics[width=4.5cm]{figures/graph_class_diagram.png}
\end{center}
\caption[Diagramma delle classi del package \texttt{graph}]{Diagramma delle classi del 
package \texttt{graph}: il diagramma mostra le correlazioni che intercorrono tra le
sue classi e il package \texttt{algorithm}.}
\label{graph_class_diagram}
\end{figure}

In particolare, il package \texttt{algorithm }comprende le classi Java necessarie all'implementazione 
degli algoritmi di subgraph matching:

\begin{itemize}

\item la classe \texttt{RIMatch} implementa gli algoritmi di matching RI e RI-DS;

\item la classe \texttt{MatchInst} consente di comparare due match individuati da RI o RI-DS;

\item la classe \texttt{BfsPath} ha infine il compito di implementare l'algoritmo BFS 
\cite{cormen2009introduction} necessario per effettuare la visita ricorsiva del grafo target allo scopo
di calcolare i cammini approssimati.

\end{itemize} 

La comparazione delle etichette dei vertici e degli archi in caso di query esatte o approssimate
è delegata rispettivamente a 4 classi Java che estendono l'interfaccia \texttt{AttrComparator}:

\begin{itemize}

\item \texttt{ExactNodeComparator} e \texttt{ExactEdgeComparator} hanno il compito di effettuare
la comparazione esatta tra vertici e archi del grafo;

\item \texttt{ApproxNodeComparator} e \texttt{ApproxEdgeComparator} hanno il compito di effettuare 
la comparazione approssimata, in caso di query contenenti wildcards.

\end{itemize}

L'operazione di ricerca dei match su un grafo target a partire da un grafo query di input è
eseguita in NetMatch* attraverso un task asincrono implementato dalla classe \texttt{MatchTask} 
che estende a sua volta la classe \texttt{AbstractTask}, definita nelle API di Cytoscape.

Il package \texttt{algorithm} comprende inoltre due subpackages denominati rispettivamente
\texttt{significance} e \texttt{metrics}: \texttt{significance}, in particolare, comprende una classe 
\texttt{RandomGenerator} che implementa i 7 algoritmi di generazione di reti random tramite i modelli 
definiti nel Capitolo~\ref{cap:chapter3}; nel package \texttt{metrics} sono invece incluse 
le classi Java adibite al calcolo delle metriche definite nella Sezione~\ref{sec:significance}.

%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=14cm]{figures/significance_class_diagram.png}
%\end{center}
%\caption[Diagramma delle classi del sistema di gestione della generazione delle reti random.]
%{Diagramma delle classi del sistema di gestione della generazione delle reti random.}
%\label{significance_class_diagram}
%\end{figure}

Al fine di ottimizzare le operazioni di matching e di attraversamento dei grafi, NetMatch* adopera
una conversione della struttura dati destinata da Cytoscape alla rappresentazione delle network,
denominata \texttt{CyNetwork}, in una apposita struttura, definita nella classe \texttt{Graph}
del package \texttt{graph}. L'operazione di conversione è delegata ad un task asincrono e
gestita da una apposita classe denominata \texttt{GraphLoader}. Infine, le classi Java \texttt{Node} e 
\texttt{Edge} implementano rispettivamente le strutture dati per la definizione dei vertici e degli 
archi del grafo.

\section{Interfaccia grafica}
L'interfaccia utente di NetMatch* è stata sviluppata avvalendosi del design pattern Model-View-Controller 
\cite{wiki:computer}. In particolare, la componente Model gestisce la rappresentazione dei dati 
relativi alle elaborazioni prodotte dal modulo principale del software. La componente View gestisce
l'implementazione dei pannelli grafici di visualizzazione delle opzioni di input e di visualizzazione
in output dei risultati delle elaborazioni. La componente Controller, infine, assicura la corretta
comunicazione tra le componenti Model e View.

\subsection{Componente Model}
La componente Model (\figurename~\ref{model_component_class_diagram}) di NetMatch* gestisce la 
rappresentazione dei dati relativi alle elaborazioni prodotte dal modulo principale del software. 

\begin{figure}[H]
\begin{center}
\includegraphics[width=5cm]{figures/model_component_class_diagram.png}
\end{center}
\caption[Diagramma delle classi relativo alla componente Model]{Diagramma delle classi relativo alla
componente Model.}
\label{model_component_class_diagram}
\end{figure}

Le classi che la compongono sono \texttt{ResultsTableModel} che ha il compito di gestire la
rappresentazione dei dati da mostrare nel pannello di output e dalle classi 
\texttt{SpringEmbeddedLayout} e \texttt{NodeDistances} che implementano le routine per la
definizione dei layout di visualizzazione grafica dei match individuati dall'algoritmo.

\subsection{Componente View}
La componente View (\figurename~\ref{view_component_class_diagram}) gestisce l'implementazione 
dei pannelli di input e di output di NetMatch*:
in particolare, \texttt{WestPanel} definisce l'interfaccia del pannello laterale
sinistro di input dell'applicazione, contenente le tre schede che consentono di selezionare
le opzioni rispettivamente di Matching, Significance e Motifs. La gestione del pannello
dei risultati è delegata invece ad un apposita classe denominata \texttt{ResultsPanel}.

\begin{figure}[H]
\begin{center}
\includegraphics[width=5cm]{figures/view_component_class_diagram.png}
\end{center}
\caption[Diagramma delle classi relativo alla componente View]{Diagramma delle classi relativo alla
componente View.}
\label{view_component_class_diagram}
\end{figure}

\subsection{Componente Controller} 
La comunicazione tra le componenti Model e View è garantita dalla componente Controller che ha
il compito di definire i task asincroni da avviare per l'esecuzione delle principali operazioni
supportate da NetMatch*. In particolare, essa è composta dalle seguenti classi Java, che estendono
la classe astratta \texttt{AbstractTask} definita nelle API di Cytoscape. Tra queste, la classe 
\texttt{MatchTask} ha il compito di avviare il task di esecuzione dell'algoritmo di subgraph matching.
Sono inoltre definite ulteriori classi che gestiscono i task per l'avvio della verifica della significatività
rispetto ai modelli di randomizzazione supportati. 

Infine, sono definiti i task che consentono di impostare il layout di visualizzazione dei dati di input 
mostrati nel pannello principale i valori delle etichette dei grafi query:

\begin{itemize}

\item la classe \texttt{NetworkLayoutTask} gestisce il layout di visualizzazione delle network 
correntemente caricate nel pannello principale di Cytoscape;

\item la classe \texttt{MotifLayoutTask} gestisce i layout di visualizzazione dei motivi definiti
nella Sezione~\ref{sec:significance}.

\item la classe \texttt{EditNodeLabelTask} e \texttt{EditEdgeLabelTask} gestiscono rispettivamente
i task asincroni che consentono di impostare i valori delle etichette esatte o approssimate per i nodi 
e per gli archi dei grafi query;

\end{itemize}

%In \figurename~\ref{results_class_diagram} è mostrato il diagramma delle classi relativo
%all'interazione tra le classi \texttt{ResultsPanel}, relativa alla componente View, e 
%\texttt{ResultsTableModel} relativa alla componente Controller.

%\begin{figure}[H]
%\begin{center}
%\includegraphics[width=14cm]{figures/results_class_diagram.png}
%\end{center}
%\caption[Diagramma delle classi del sistema di gestione del pannello di visualizzazione dei 
%risultati.]{Diagramma delle classi del sistema di gestione del pannello di visualizzazione 
%dei risultati.}
%\label{results_class_diagram}
%\end{figure}

