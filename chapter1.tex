\chapter{Definizioni preliminari}
\vspace{4cm}

In questo capitolo saranno presentate alcune definizioni preliminari di base\linebreak relative alla 
teoria dei grafi \cite{bollobas2013modern, west2001introduction} e delle reti complesse 
\cite{van2010graph}.

\begin{defin}[Grafo]
Un grafo è una coppia $G = (V,E)$ dove $V=\{v_0,v_1,\ldots,\\v_{|V|}\}$ è l'insieme dei vertici (o nodi) 
ed $E\subseteq V\times V$ è l'insieme degli archi di $G$, ossia l'insieme delle
connessioni tra i vertici di $G$.
\end{defin}

\begin{defin}[Grafo non orientato]
Un grafo $G = (V,E)$ si dice non orientato se $E$ è un insieme di coppie non ordinate di vertici.
Dati due vertici $v_i,v_j\in V$, un arco di un grafo non orientato si indica con $(v_i,v_j)$ e si 
dice \textit{incidente} ad entrambi i vertici $v_i$ e $v_j$.
\end{defin}

\begin{defin}[Grafo orientato]
Un grafo $G = (V,E)$ si dice orientato se $E$ è un insieme di coppie ordinate di vertici. Dati due 
vertici $v_i,v_j\in V$, un arco di un grafo orientato si indica con $\overrightarrow{(v_i,v_j)}$ e 
si dice \textit{incidente} da $v_i$ in $v_j$.
\end{defin}

\begin{defin}[Multigrafo]
Un multigrafo è un grafo $G=(V,E)$ contenente archi multipli (archi che connettono stesse coppie di
nodi) e cappi (archi che collegano nodi a loro stessi).
\end{defin}

Esempi di rappresentazione di grafi non orientati ed orientati sono mostrati in 
\figurename~\ref{fig:figure1}.

\begin{figure}[!h]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.75, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [circle, draw = black, fill = black]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\node[node_style] (v1) at (-2,2) {};
	\node[node_style] (v2) at (2,2) {};
	\node[node_style] (v3) at (4,0) {};
	\node[node_style] (v4) at (2,-2) {};
	\node[node_style] (v5) at (-2,-2) {};
	\node[node_style] (v6) at (-4,0) {};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v4) edge (v5);
	\draw[edge_style] (v5) edge (v6);
	\draw[edge_style] (v6) edge (v1);
	\draw[edge_style] (v5) edge (v1);
	\draw[edge_style] (v5) edge (v2);
	\draw[edge_style] (v4) edge (v2);
	\end{tikzpicture}}
\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.75, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [circle, draw = black, fill = black]
	\tikzstyle{edge_style} = [->, draw = black, semithick]
	\node[node_style] (v1) at (-2,2) {};
	\node[node_style] (v2) at (2,2) {};
	\node[node_style] (v3) at (4,0) {};
	\node[node_style] (v4) at (2,-2) {};
	\node[node_style] (v5) at (-2,-2) {};
	\node[node_style] (v6) at (-4,0) {};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v4) edge (v5);
	\draw[edge_style] (v5) edge (v6);
	\draw[edge_style] (v6) edge (v1);
	\draw[edge_style] (v5) edge (v1);
	\draw[edge_style] (v5) edge (v2);
	\draw[edge_style] (v4) edge (v2);
	\end{tikzpicture}}
\caption[Rappresentazione grafica di un grafo orientato e non orientato]{Rappresentazione 
grafica di un grafo non orientato (a) e orientato (b) aventi 6 nodi e 9 archi. Nel grafo 
orientato i nodi adiacenti sono connessi tramite frecce che indicano la direzione di 
ciascun collegamento. I grafi non contengono archi multipli o cappi.}
\label{fig:figure1}
\end{figure}

\begin{defin}[Grafo denso]
Un grafo $G=(V,E)$ è denso se $|E|=\mathscr{O}(|V|^2)$.
\end{defin}

\begin{defin}[Grafo sparso]
Un grafo $G=(V,E)$ è sparso se $|E|\ll |V|^2$.
\end{defin}

\begin{defin}[Grafo etichettato]
Siano $G=(V,E)$ un grafo, $\Sigma$ un alfabeto di etichette, $\alpha:V\rightarrow\Sigma$ una funzione che 
assegna ad ogni nodo di $G$ un'etichetta e $\beta:E\rightarrow\Sigma$ una funzione di che assegna
ad ogni arco di $G$ un'etichetta, allora $G=(V,E,\alpha,\beta)$ è detto grafo etichettato.

$G=(V,E,\alpha)$ è un grafo nodi con etichettati e $G=(V,E,\beta)$ è un grafo con archi etichettati.
\end{defin}

\begin{defin}[Matrice di adiacenza]
Una matrice di adiacenza $A(a_{ij})$ di un grafo $G=(V,E)$ è una matrice $|V|\times|V|$ tale che

\begin{displaymath}
a_{ij} = \left\{
\begin{array}{rl}
1 & \text{se } (v_i,v_j) \in E,\\[0.5em]
0 & \text{altrimenti}.
\end{array} \right.
\end{displaymath}

La diagonale principale della matrice di adiacenza contiene solo valori uguali a zero.
\end{defin}

\begin{defin}[Matrice delle distanze]
Una matrice delle distanze $D(d_{ij})$ di un grafo $G=(V,E)$ è una matrice $|V|\times|V|$ tale che
ogni suo elemento $d_{ij}$ è uguale alla distanza del cammino minimo tra i vertici $v_i,v_j\in V$.

La diagonale principale della matrice delle distanze contiene solo valori uguali a zero.
\end{defin}

In \figurename~\ref{fig:adj} è mostrato un grafo non orientato, la sua rappresentazione mediante
matrice di adiacenza e la sua matrice delle distanze.

\begin{figure}[!h]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.5, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [circle, draw = black, fill = white, semithick]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\node[node_style] (v1) at (0,4) {1};
	\node[node_style] (v2) at (4,4) {2};
	\node[node_style] (v3) at (6,2) {3};
	\node[node_style] (v4) at (4,0) {4};
	\node[node_style] (v5) at (0,0) {5};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v1) edge (v5);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v2) edge (v4);
	\draw[edge_style] (v2) edge (v5);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v4) edge (v5);
	\end{tikzpicture}}
\qquad\quad
\subfigure[]{%
	\begin{tabular}[b]{r|ccccc|}
	\multicolumn{1}{r}{}
 	& \multicolumn{1}{c}{1}
 	& \multicolumn{1}{c}{2}
 	& \multicolumn{1}{c}{3}
 	& \multicolumn{1}{c}{4}
 	& \multicolumn{1}{c}{5} \\
	\cline{2-6}
	1 & 0 & 1 & 0 & 0 & 1 \\
	2 & 1 & 0 & 1 & 1 & 1 \\
	3 & 0 & 1 & 0 & 1 & 0 \\
	4 & 0 & 1 & 1 & 0 & 1 \\
	5 & 1 & 1 & 0 & 1 & 0 \\
	\cline{2-6}
	\end{tabular}}
\qquad\quad
\subfigure[]{%
	\begin{tabular}[b]{r|ccccc|}
	\multicolumn{1}{r}{}
 	& \multicolumn{1}{c}{1}
 	& \multicolumn{1}{c}{2}
 	& \multicolumn{1}{c}{3}
 	& \multicolumn{1}{c}{4}
 	& \multicolumn{1}{c}{5} \\
	\cline{2-6}
	1 & 0 & 1 & 2 & 2 & 1 \\
	2 & 1 & 0 & 1 & 1 & 1 \\
	3 & 2 & 1 & 0 & 1 & 2 \\
	4 & 2 & 1 & 1 & 0 & 1 \\
	5 & 1 & 1 & 2 & 1 & 0 \\
	\cline{2-6}
	\end{tabular}}
\caption[Rappresentazione di un grafo mediante matrice di adiacenza e matrice delle distanze]
{Rappresentazione di un grafo mediante matrice di adiacenza e matrice della distanze: il 
grafo non orientato avente 5 vertici e 7 archi (a) è rappresentato mediante una matrice 
di adiacenza (b). In figura (c) è mostrata la corrispondente matrice delle distanze.}
\label{fig:adj}
\end{figure}

\begin{defin}[Sottografo]
Un grafo $G'=(V',E')$ è un sottografo di $G=(V,E)$ se $V'\subseteq V$ e $E'\subseteq E$.
\end{defin}

\begin{defin}[Sottografo indotto]
Dati un grafo $G=(V,E)$ e un suo sottografo $G'=(V',E')$, si dice che $G'$ è un sottografo indotto
da $V'$ e lo si denota con $G[V']$ se $G'$ contiene gli stessi archi di $G$ che collegano due nodi
in $V'$.
\end{defin}

\begin{defin}[Insieme dei vicini di un nodo]
Dato un grafo $G=(V,E)$ si definisce insieme dei vicini di un nodo $v\in V$ l'insieme $N(G,v)=
\{v'\in V:(v,v')\in E\}$. 

Se il grafo è orientato si definisce l'insieme dei vicini successori di un nodo $v\in V$ come 
$N_{\text{out}}(G,v)=\{v'\in V:\overrightarrow{(v,v')}\in E\}$ e l'insieme dei vicini predecessori come 
$N_{\text{in}}(G,v)=\{v'\in V:\overrightarrow{(v',v)}\in E\}$.
\end{defin}

\begin{defin}[Sottografo dei vicini]
Sia $G=(V,E)$ un grafo e sia $N(G,v)$ l'insieme dei vicini di un certo nodo $v\in V$. Allora si definisce 
sottografo dei vicini di $v$ il sottografo indotto da $N(G,v)$.
\end{defin}

\begin{defin}[Cammino in un grafo]
Un cammino di lunghezza $k$ da un vertice $u$ a un vertice $u'$ in un grafo $G=(V,E)$ è una 
sequenza $(v_0,v_1,\ldots,v_k)$ di vertici tale che $u=v_0,u'=v_k$ e $(v_{i-1},v_i)\in E$ con
$i=1,2,\ldots k$.

La lunghezza del cammino è il numero di archi del cammino.

Se esiste un cammino $p$ da $u$ a $u'$ è possibile dire che $u'$ è raggiungibile da $u$ \linebreak
tramite $p$.
\end{defin}

\begin{defin}[Cammino minimo]
Un cammino da un vertice $u$ a un vertice $u'$ in un grafo $G=(V,E)$ è detto minimo se è
il cammino di lunghezza minore tra $u$ e $u'$. 
\end{defin}

\begin{defin}[Diametro]
Il diametro di un grafo $G=(V,E)$ è indicato come $\text{Diam}(G)$ ed è dato dalla lunghezza massima
di tutti i cammini minimi tra coppie di vertici di $G$.
\end{defin}

\begin{defin}[Lunghezza caratteristica di un cammino]
Dato un grafo $G=(V,E)$ si definisce lunghezza caratteristica di un cammino $L$ o, più semplicemente, 
cammino caratteristico, la media delle distanze tra ogni coppia di nodi $v_i,v_j\in V$:

\begin{displaymath}
L=\frac{1}{|V|(|V|-1)}\sum_{v_i,v_j\in V,i\neq j}d_{ij}
\end{displaymath}

\end{defin}

\begin{defin}[Grafo connesso]
Un grafo $G=(V,E)$ si dice connesso se per ogni coppia di nodi distinti $(v_i,v_j)\in V$ esiste un 
cammino da $v_i$ a $v_j$. In caso contrario il grafo si dice non connesso o disconnesso.
\end{defin}

\begin{defin}[Grafo completo]
Un grafo $G=(V,E)$ si dice completo se per ogni coppia di nodi distinti $(v_i,v_j)\in V$ esiste un
arco che collega $v_i$ a $v_j$
\end{defin}

\begin{defin}[Componente]
Dato un grafo $G=(V,E)$ una componente (connessa) è un sottografo di $G$ in cui qualsiasi coppia
di nodi è connessa tramite un cammino.
\end{defin}

\begin{defin}[Isomorfismo fra grafi]
Due grafi $G=(V,E)$ e $G'=(V',E')$ si dicono isomorfi se esiste una corrispondenza biunivoca 
$f:V\rightarrow V'$ tale che $(u,v)\in E$ se e solo se $(f(u),f(v))\in E'$. In altre parole è possibile
rietichettare i vertici di $G$ con vertici di $G'$, mantenendo i corrispondenti archi di $G$ e di
$G'$.
\end{defin}

In \figurename~\ref{fig:figure2} è mostrato un esempio di isomorfismo fra due grafi.

\begin{figure}[!h]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 1.25, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [fill = white]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\node[node_style] (v1) at (-2,0) {A};
	\node[node_style] (v2) at (0,0) {B};
	\node[node_style] (v3) at (0,2) {C};
	\node[node_style] (v4) at (2,2) {D};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v2) edge (v4);
	\end{tikzpicture}}
\qquad
\subfigure[]{%
	\begin{tikzpicture}[scale = 1.25, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{node_style} = [fill = white]
	%\tikzstyle{edge_style} = [radius = 5pt, draw = black, semithick]
	\tikzstyle{edge_style} = [draw = black, semithick]
	\node[node_style] (v1) at (-4,0) {1};
	\node[node_style] (v2) at (-2,0) {2};
	\node[node_style] (v3) at (0,0) {3};
	\node[node_style] (v4) at (2,0) {4};
	\draw[edge_style] (v1) edge (v2);
	\draw[edge_style] (v2) edge (v3);
	\draw[edge_style] (v3) edge (v4);
	\draw[edge_style] (v1) edge[bend left] (v3);
	\end{tikzpicture}}
\caption[Esempio di isomorfismo fra grafi]{Esempio di isomorfismo fra due grafi.}
\label{fig:figure2}
\end{figure}

\begin{defin}[Isomorfismo fra sottografi]\label{def:subgraph_matching}
Un grafo $G=(V,E)$ è un sottografo isomorfo di $G'=(V',E')$ se esiste $f:V\rightarrow V'$ tale che
$(u,v)\in E$ se e solo se $(f(u),f(v))\in E'$. In altre parole è possibile rietichettare i vertici
di $G$ con i vertici di $G'$, mantenendo i corrispondenti archi in $G$.
\end{defin}

In \figurename~\ref{fig:figure3} è mostrato un esempio di isomorfismo fra sottografi.

\begin{figure}[!h]
\centering
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.5, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{black_node_style} = [circle, fill = black]
	\tikzstyle{grey_node_style} = [circle, fill = {rgb,255:red,128; green,128; blue,128}]
	\tikzstyle{black_edge_style} = [draw = black, semithick]
	\tikzstyle{grey_edge_style} = [draw = {rgb,255:red,128; green,128; blue,128}, semithick]
	\node[black_node_style] (v1) at (-8,4) {};
	\node[black_node_style] (v2) at (-6,2) {};
	\node[black_node_style] (v3) at (-6,0) {};
	\node[grey_node_style] (v4) at (-4,4) {};
	\node[grey_node_style] (v5) at (-4,-2) {};
	\node[grey_node_style] (v6) at (-2,2) {};
	\node[grey_node_style] (v7) at (-2,0) {};
	\node[grey_node_style] (v8) at (0,0) {};
	\node[grey_node_style] (v9) at (2,2) {};
	\node[grey_node_style] (v10) at (2,0) {};
	\node[grey_node_style] (v11) at (4,0) {};
	\node[black_node_style] (v12) at (6,2) {};
	\node[black_node_style] (v13) at (6,-2) {};
	\draw[black_edge_style] (v1) edge (v2);
	\draw[black_edge_style] (v2) edge (v4);
	\draw[black_edge_style] (v2) edge (v3);
	\draw[black_edge_style] (v3) edge (v5);
	\draw[grey_edge_style] (v4) edge (v6);
	\draw[grey_edge_style] (v6) edge (v7);
	\draw[grey_edge_style] (v5) edge (v7);
	\draw[grey_edge_style] (v7) edge (v8);
	\draw[grey_edge_style] (v8) edge (v10);
	\draw[grey_edge_style] (v10) edge (v9);
	\draw[grey_edge_style] (v10) edge (v11);
	\draw[black_edge_style] (v11) edge (v12);
	\draw[black_edge_style] (v11) edge (v13);	
	\end{tikzpicture}}
\qquad\quad
\subfigure[]{%
	\begin{tikzpicture}[scale = 0.5, shorten >= 1pt, auto, node distance = 3cm, ultra thick]
	\tikzstyle{grey_node_style} = [circle, fill = {rgb,255:red,128; green,128; blue,128}]
	\tikzstyle{grey_edge_style} = [draw = {rgb,255:red,128; green,128; blue,128}, semithick]
	\node[grey_node_style] (v1) at (-4,4) {};
	\node[grey_node_style] (v2) at (-4,-2) {};
	\node[grey_node_style] (v3) at (-2,2) {};
	\node[grey_node_style] (v4) at (-2,0) {};
	\node[grey_node_style] (v5) at (0,0) {};
	\node[grey_node_style] (v6) at (2,2) {};
	\node[grey_node_style] (v7) at (2,0) {};
	\node[grey_node_style] (v8) at (4,0) {};
	\draw[grey_edge_style] (v1) edge (v3);
	\draw[grey_edge_style] (v3) edge (v4);
	\draw[grey_edge_style] (v2) edge (v4);
	\draw[grey_edge_style] (v4) edge (v5);
	\draw[grey_edge_style] (v5) edge (v7);
	\draw[grey_edge_style] (v7) edge (v6);
	\draw[grey_edge_style] (v7) edge (v8);
	\end{tikzpicture}}
\caption[Esempio di isomorfismo fra sottografi]{Esempio di isomorfismo fra sottografi: il grafo
rappresentato in (b) è contenuto nel grafo rappresentato in (a).}
\label{fig:figure3}
\end{figure}

\begin{defin}[Grado di un nodo]
Dato un grafo $G=(V,E)$, il grado (o connettività) $k_{v_i}$ di un nodo $v_i\in V$ è il numero di archi 
incidenti ed è definito in termini della matrice di adiacenza $A(a_{ij})$ come:

\begin{displaymath}
k_{v_i} = \sum_{v_j\in V}a_{ij}.
\end{displaymath}

Se il grafo è diretto occorre distinguere, per un certo nodo $v_i\in V$, il grado entrante
$k_{v_i}^{\text{in}}$ (il numero di archi entranti) e il grado uscente $k_{v_i}^{\text{out}}$ (il numero di 
archi uscenti), che valgono rispettivamente $k_{v_i}^{\text{in}} = \sum_{v_j\in V}a_{ji}$ e 
$k_{v_i}^{\text{out}} = \sum_{v_j\in V}a_{ij}$.

Il grado totale è definito come $k_{v_i}=k_{v_i}^{\text{out}}+k_{v_i}^{\text{in}}$.
\end{defin}

\begin{defin}[Distribuzione di grado]
Dato un grafo $G=(V,E)$ si definisce distribuzione di grado $P(k)$ la probabilità che un certo nodo
selezionato casualmente abbia grado $k$.

Se il grafo è diretto è possibile definire la distribuzione di probabilità per il grado entrante
e per il grado uscente, indicate rispettivamente con $P(k^{\text{in}})$ e $P(k^{\text{out}})$.
\end{defin}

%\begin{defin}[Grado medio dei vicini di un nodo]
%Dato un grafo $G=(V,E)$ e un nodo $v_i\in V$ si definisce grado medio dei vicini di un nodo 
%\cite{boccaletti2006complex} la quantità:

%\begin{displaymath}
%\langle k_{v_i}\rangle=\frac{1}{k_{v_i}}\sum_{v_j\in N(G,v_i)}k_{v_j}
%=\frac{1}{k_{v_i}}\sum_{j=1}^{|V|}a_{ij}k_{v_j}
%\end{displaymath}

%\end{defin}

\begin{defin}[Grado medio dei vicini dei nodi con grado $k$]
Dato un grafo $G=(V,E)$ e un nodo si definisce grado medio dei vicini dei nodi \cite{pastor2001dynamical} 
con grado $k$ la quantità:

\begin{displaymath}
\langle k_{nn}\rangle=\sum_{k'}P(k'|k).
\end{displaymath} 

\end{defin}

\begin{defin}[Grado in eccesso]
Dato un grafo $G=(V,E)$, il grado in eccesso \cite{newman2002assortative} $q_{v_i}$ di nodo $v_i\in V$ 
è il grado del nodo \emph{meno uno}, ossia il numero di archi adiacenti il nodo escluso l'arco 
da cui si è arrivati.
\end{defin}

\begin{defin}[Distribuzione di grado in eccesso]
Dato un grafo $G=(V,E)$ definiamo la distribuzione di grado in eccesso \cite{newman2002assortative} 
$Q(k)$ come:

\begin{displaymath}
Q(k)=\frac{(k+1)P(k)+1}{\sum_jjP(j)}.
\end{displaymath}

\end{defin}

\begin{defin}[Distribuzione di probabilità disgiunta del grado in eccesso]
Dato un grafo $G=(V,E)$ definiamo la distribuzione di probabilità disgiunta del grado in
eccesso di due vertici \cite{callaway2001randomly} $v_i,v_j\in V$ come una quantità ${e_{v_iv_j}}$ che gode
delle seguenti proprietà:

\begin{displaymath}
\sum_{ij}e_{v_iv_j}=1,\qquad\sum_ie_{v_iv_j} = Q(j).
\end{displaymath}

\end{defin}

\begin{defin}[Momento della distribuzione]
Dato un grafo $G=(V,E)$ definiamo momento $n$-esimo della sua distribuzione di grado 
\cite{boccaletti2006complex} la quantità:

\begin{displaymath}
\langle k_n\rangle = \sum_{k}k^n P(k).
\end{displaymath}

\end{defin}

\begin{defin}[Grado medio]
Dato un grafo $G=(V,E)$ definiamo grado medio $\langle k\rangle$ \cite{boccaletti2006complex} 
il primo momento della sua distribuzione di grado ossia $\langle k_1\rangle$.
\end{defin}

\begin{defin}[Coefficiente di clustering locale]
Dato un grafo $G=(V,E)$ e un suo nodo $v_i\in V$ definiamo coefficiente di clustering
locale \cite{watts1998collective} $c_{v_i}$ del nodo $v_i$ il rapporto tra il 
numero di archi $|E|$ del grafo e il numero massimo possibile di archi in $G_i$:

\begin{displaymath}
c_{v_i}= \frac{2|E|}{k_{v_i}(k_{v_i}-1)}.
\end{displaymath}
\end{defin}

\begin{defin}[Coefficiente di clustering]
Dato un grafo $G=(V,E)$ definiamo coefficiente di clustering $C$ la media dei 
coefficienti di clustering $c_{v_i}$ di tutti i nodi $v_i\in V$:

\begin{displaymath}
C= \langle c \rangle=\frac{1}{|V|}\sum_{v_i\in V}c_{v_i}.
\end{displaymath}

\end{defin}

\begin{defin}[Coefficiente di assortatività]
Dato un grafo $G=(V,E)$ definiamo coefficiente di assortatività \cite{newman2002assortative}
la quantità:

\begin{displaymath}
r = \frac{1}{\sigma^2_q}\sum_{jk}jk(e_{v_jv_k}-Q(j)Q(k)),
\end{displaymath}

dove $\sigma^2_q$ è la varianza della distribuzione $Q(k)$, ossia:

\begin{displaymath}
\sigma^2_q=\sum_kk^2Q(k)-\Big[\sum_kkQ(k)\Big]^2.
\end{displaymath}

\end{defin}

%\begin{defin}[Norma]
%Una norma su uno spazio vettoriale $\mathbb{R}^d$ è una funzione $\lVert\cdot\rVert$ che gode delle
%seguenti proprietà:

%\begin{itemize}

%\item $\lVert x\rVert\geq 0\text{ }\forall x\in\mathbb{R}^d$ e $\lVert x\rVert=0$ se e solo se $x=0$;

%\item $\lVert\lambda x\rVert=|\lambda| \lVert x\rVert$ per ogni scalare $\lambda$;

%\item $\lVert x+y\rVert\leq\lVert x\rVert+\lVert y\lVert$ per ogni $x,y\in\mathbb{R}^d$.

%\end{itemize}
%\end{defin}
