/**
 *
 * @author Fabio Rinnone
 */

import java.io.*;

public class FixIndex {

	private static final String tocFileName = "thesis.toc";
	private static final String auxFileName = "thesis.aux";
	private static final String outFileName = "thesis.out";
	private static final String tokenIndex = "Indice";
	private static final String tokenFigures = "Elenco delle figure";
	private static final String tokenBiblio = "Bibliografia";
	private static final String tokenNumLine = "\\numberline {}";

	public static void main(String[] args) {

		try {
			File tocFile = new File(tocFileName);
			File tmpFile = new File("tmp.txt");
			
			copyFile(tocFile, tmpFile);
			
			File auxFile = new File(auxFileName);		
			tmpFile = new File("tmp.txt");

			copyFile(auxFile, tmpFile);

			File outFile = new File(outFileName);
			tmpFile = new File("tmp.txt");
			
			copyFile(outFile, tmpFile);
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
		
		System.exit(0);
	}

	private static void copyFile(File inputFile, File tmpFile) throws Exception {
		
		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		BufferedWriter writer = new BufferedWriter(new FileWriter(tmpFile));
		
		String line;
		while((line = reader.readLine()) != null) {
			if (!line.contains(tokenIndex) && !line.contains(tokenFigures) && !line.contains(tokenBiblio)) {
				writer.write(line + System.getProperty("line.separator"));
			}
			if (line.contains(tokenBiblio)) {
				System.out.println("Token: " + tokenNumLine);
				System.out.println("Line: " + line);
				String lineReplaced = line.replace(tokenNumLine, "");
				System.out.println("Line replaced: " + lineReplaced);
				writer.write(line.replace(tokenNumLine, "") + System.getProperty("line.separator"));
			}
		}

		writer.close();
		reader.close();	

		tmpFile.renameTo(inputFile);	
	}
}

