# Makefile per tesi in LaTeX

default: all

all:
	pdflatex thesis.tex
	bibtool -v -r options -i bibliography.bib -o biblio.bib
	bibtex thesis
	pdflatex thesis.tex
	#workaround
	javac FixIndex.java
	java FixIndex
	pdflatex thesis.tex
	
clean:
	rm -f *.aux
	rm -f *.log
	rm -f *.toc
	rm -f *.loa
	rm -f *.lot
	rm -f *.lof
	rm -f *.idx
	rm -f *.bbl
	rm -f *.blg
	rm -f *.idx
	rm -f *.ilg
	rm -f *.ind
	rm -f *.class
	rm -f *.bcf
	rm -f *.out
	rm -f *.loa
	rm -f *.run.xml
	rm -f *~
	rm -f biblio.bib

erase:
	make clean
	rm -f *.dvi?
	rm -f *.ps?
	rm -f *.pdf?

help:
	@echo "make [all]: genera il file .pdf della tesi"
	@echo "make clean: elimina i file non necessari"
	@echo "make erase: elimina i file non necessari e il file .pdf"
	@echo "Leggere anche README"
